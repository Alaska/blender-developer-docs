# Assets

First read the [2020 blog post](https://code.blender.org/2020/03/asset-manager/) to see the complete asset manager design proposal. Here you can find a breakdown explanation of how this proposal came to be.

## Design

At the core of this project, the design was defined as a single line: *"An asset is a data-block with meaning"*.

On top of this big-picture design, it was also proposed that users should be in control of managing the assets (this was partily re-visited much later though). These constrains lead to a few implications:

* Users don't create an asset
   * Instead they can set an existing data-block as an asset by adding meaning (meta-data) to it.
* Users manage assets the same way they manage data-blocks
  * Assets can rely on linked data-blocks.
  * Assets can package all its depedencies.
  * A .blend file can contain multiple assets.

## Visualization

![Database and Assets](../../../../images/design_examples_database-asset.png)

This visualization builds on top of the [View Layer](https://code.blender.org/2017/09/view-layers-and-collections/) design.
It tries to create a mental model of how assets are interwined inside the main Blender data-base.

The following tries to make it clear that there is little difference between a regular data-block and an asset:
![Asset Meta data](../../../../images/design_example_asset_metadata.png){style="background-color:#1f2129"}

## Life cycle of an asset

[“Create, edit, share and use it.”](https://code.blender.org/2020/03/asset-manager/#life-cycle-of-an-asset)

During the design phase User Story Mapping was used to understand what are the possible actions a user may do with an asset, and ways to group them:

* Create
* Edit
* Share
* Use

Having these clear defined group of actions helped to make sure any feature which was discussed was understood within one of these context. It also helped the user interface organization, where the separation between the group of actions were desired.
