# Frequently Asked Questions

## Troubleshooting

### Why did my Blender compile fail?

See [Resolving Build Failures](../building_blender/troubleshooting.md).

There may be many reasons why it failed, but here are some common causes.
Also remember to look for the first error that is printed, as this is
usually the root cause, further errors may only be a result of this.

### Why did my Blender compile with warnings?

While Blender can compile warning-free on some platforms and configurations,
at any given time there may be a handful of warnings, especially due to
recent changes.

If you see warnings while compiling as a user, you can assume Blender
developers are aware of them and they're almost certainly *false-positives*,
or at least not causing real-world problems.

If you are a developer and warnings show in the area you are working one,
and especially if your changes introduced a warning, please carefully check
and resolve them.

## Technical

### Which language is Blender written in?

Short answer: C++, Python and GLSL

- Python for user interface layout, simple tools, keymaps, presets and add-ons.
- GLSL for shaders.
- C++ for the rest of the code. Originally most code was C, but only a small
  amount remains now.

### Why C++ and Python?

Part of it is historic, part of it is convention and choice of the
current developers.

Early on when Blender was developed (1990's), C was very common for
developing graphics applications. C++ compilers were expensive, where C
compilers were free.

Later most code was changed to C++ because developers wanted to take
advantage of its features, other graphics libraries also use it,
and because transitioning from C is straightforward.

Python was selected because of its great community. Later on it became
the main scripting language in graphics software.

Currently this works fairly well and we don't have plans to rewrite
large parts of Blender in other languages.

### Can support for other scripting languages be added?

Supporting multiple languages is not something we're currently interested in.

Embedding another language would not just have a significant initial
implementation cost, but a large ongoing maintenance overhead for the project
as a whole.

As Python is the most common scripting language in graphics software, there
is currently no compelling reason, besides developer preferences, to add
support for other languages or move away from Python.

### Can a C++ plugin API be added?

For historical reasons, adding such an API is difficult. Blender was not
designed as an extensible software. Rather adding new functionality often
requires making changes across many files and modules, and internal APIs
are often designed for specific usage rather than being general purpose
enough as a public API for plugins.

While work is ongoing to improve the Blender implementation in this regard,
realistically, adding support for a C++ plugin API is unlikely to happen
anytime soon.

### Can support for implementing nodes in Python be added?

Python is not well suited for fast interactive operations on large data
sets such as pixels or vertices.

Even in cases where computation can be optimized, Python has a global
interpreter lock (GIL), making any operations which use it single
threaded.

More realistic would be support for implementing them in C/C++, or using
GLSL or other shading languages. However for the same reasons as the lack
of a C++ plugin API, this first requires some architectural changes to
Blender.

### Why does Blender use its own user interface toolkit instead of native ones?

The reason for this is primarily historical. Blender development started
in the early 1990s and advanced cross platform user interface toolkits
did not exist.

Switching to another user interface toolkit now would involved a large,
coordinated effort of multiple developers over multiple years. It would
also involve some risk as popularity of toolkits comes and goes, and making
the wrong choice would require another rewrite in the future.

On the one hand, implementing and maintaining has a significant cost. But it
also allows some features and customizations that would not be possible with
an off the shelf toolkit. For example the non-overlapping layout or file
browser that can handle library linking are some examples of custom features.

While much can be said about the pros and cons, the current preference is
to stay on Blender's custom UI toolkit and incrementally improve it.

### Can I use Blender's user interface toolkit in my application?

The implementation is tightly coupled to the rest of the Blender code,
and extracting it into an independent library would take a large amount
of work.

### How does Blender deal with security?

Blender does not attempt to achieve the same level of security as many
other applications (web browsers for example).

The ability to have .blend files that execute bundled Python scripts
does pose a security risk if you don't know who created the file.

The **Trusted Source** option has been added to the file so you can load
a .blend file without running scripts as a precaution to simple attacks.

However this is no protection against more advanced exploits such as
hand crafting a .blend file which uses buffer overflows to run malicious
code.

## Understanding the Code

### Where does Blender start and what happens next?

The file `source/creator/creator.c` has the `main()` routine in it.

After parsing arguments, a bunch of initialization, and reading a *.blend
file*, `main()` passes control to `WM_main()` in
`source/blender/windowmanager/intern/wm.c`. That is an infinite loop
that just processes events and notifications and draws updates on the
screen. Events and notifications are things caused either by the user
doing something (e.g., mouse click) or the internal Blender systems
signaling to other parts of Blender that something has changed and that
means something has to be recalculated, redrawn, or something else.

### Where is the user interface code?

The UI layout is mostly defined in Python but the drawing is done in C++,
and some of the UI is defined in C++ too.

`release/scripts/modules/bl_ui/`
:   Python code, for example `space_view3d.py` in that
    directory is the code for the *3D View* editor type (space).

`source/blender/editors/interface/`
:   C++ interface drawing code.

`source/blender/makesrna/intern/rna_ui_api.c`
:   The glue code defining the API python uses to create interfaces

The Python UI code mostly just lays out user-interaction widgets that
adjust properties of [operators](../../features/interface/operators.md).

### How are operators connected to C++ code?

See [navigating code from the interface](navigate_code.md#from-the-interface).

### What is this DNA and RNA stuff?

The names come from the genetic nucleic acids and hint at their
function: DNA, as in genetics, is the *code* that lets you reproduce
something. In the case of Blender, this is essentially what is written
to and read from disk as a *.blend* file - it has all of the user
settings and mesh, object, scene, etc. data. RNA, as in genetics, is a
helper to transmit the DNA *code* into the rest of the internal Blender
system.

See [DNA](../../features/core/dna.md) and [RNA](../../features/core/rna.md) documentation.

### Where are the icons?

See [Icons](../../features/interface/icons.md) documentation.

## Process

### How can users provide feedback to developers?

See [User Feedback](../communication/user_feedback.md).

### How can users help with Blender development?

Even if you don't write code, there are other ways to help.

* [Test and Provide Feedback](https://devtalk.blender.org/c/feature-design-feedback/30)
* [Write Documentation](https://docs.blender.org/manual/en/latest/contribute/index.html)
* [Triage Bugs](../bug_reports/help_triaging_bugs.md)
* [Translate Blender](../translating/translator_guide.md)
* [Modules Membership](../organization/modules.md)

### Why not release Blender when it's ready instead of rushing it every few months?

In the past this approach has been used, but as we and other large software
projects have found, it has a number of problems.

- With more developers and contributors, some volunteers and working for
  other companies, relying on tasks being finished in a fixed time frame
  does not work well.
- Since Blender is a complex application, it became increasingly difficult
  to pick the perfect moment to release all areas.
- Too long gaps between release allows code to be in a state that is
  not sufficiently tested and stabilized. There is also more pressure
  to merge features as the release date approaches, as the next one
  may be a long time away.
- Users would have to wait longer to take advantage of features that are
  ready.

Instead we have switched to a release schedule fixed in time, where
if a new feature is not ready it moves to the next release.
The main branch is intended to be always stable, while new
features are developed in branches or as experimental features that
can be enabled for testing.

With this method, developers can each work at their own pace and deliver
features when they are ready, without users having to wait too long to
use them.

This is not to say our release process is perfect, and improvements
can be made to ensure features that land are stable and well tested.

### Feature X in Application Y is Perfect! Why not implement it the same way?

Blender has its own workflows, and follows different paradigms.
For any functionality of course we need to consider the options
available - including any existing conventions.

However we also need to make sure this fits well with Blender which has
its own conventions and internal consistency. It's important to think for
ourselves, questioning the status quo since it may not be a good option
for us. We can aim to do better too!

This is not to suggest when Blender is different it's always better
or that developers always make the best design. Every case needs to
be considered individually.

### Why are contributions rejected or delayed, when they appear to be ready?

While a feature may work well to solve one particular problem, often
it's not so simple. Usually an initial implementation is half the work.
Improving, integrating, stabilizing, documenting and maintaining it
is the other half, or even more.

- A feature may work well in simple tests, but when testing more bugs
  and integration problems are revealed. What works for one user
  often does not work for another.
- The solution may be short term, while a better but more complicated
  solution is preferred. This is a balance, sometimes a short term fix
  is good. But it also means doing work twice to implement and stabilize
  the feature. Replacing it later is often difficult once the sub-optimal
  solution is entrenched in the design.
- The code may not need meet quality standards or fit properly in the design.
  Accepting too much code like this can slow down future development or
  break existing feature. And this has often happened in the past,
  where some areas of the code can become difficult to work with and see
  less development as a result.

Communicating exact reasons and expectations to contributors here is important.
