# Pipeline, Assets & I/O

## USD

- Support for import
  (blender/blender@cdef135f6f)
  and export
  (blender/blender@0c67a90e4f)
  of USDZ (compressed version of USD) was added.
- USD export changes to pass `usdchecker`
  (blender/blender@b67b84bd5d).
- Add scale and bias for exported `USD Preview Surface` normal maps
  (blender/blender@c79b55fc05).
- Convert USD camera properties to mm from USD units
  (blender/blender@f359a39d11).
- Support importing USD Shapes
  (blender/blender@72a85d976a).
- Author extents on exported `UsdGeomMesh`
  (blender/blender@5040c39d1a).

## OBJ

- New OBJ importer got "Split by Objects" and "Split by Groups" import
  settings, just like the old Python based addon had
  (blender/blender@b59982041).
- New OBJ importer can now import polylines with more than 2 vertices
  (blender/blender@cfe828b452e6).
- Removing incorrect edge recalculation made the OBJ exporter up to 1.6x
  faster
  (blender/blender@455d195d5504)

## Assets

- New "All" asset library
  (blender/blender@35e54b52e6).
  Displays contents from all other asset libraries. This asset library
  is shown in Asset Browsers by default
  (blender/blender@7ba59c8a62).
- New "Essentials" asset library
  (blender/blender@b3fb73f325)
  containing a number of assets that are now shipped with Blender. It
  includes a number of hair node groups for use with geometry nodes.  

![](../../images/Release_notes_essentials_asset_library.png){style="width:600px;"}

![](../../images/3.5_release_notes_asset_library_Preferences_UI.png){style="width:600px;"}

- The user interface for setting up asset libraries in the Preferences
  has been revamped.
  (blender/blender@0d798ef57c)
- A default import method (*Append*, *Append (Reuse Data)*, *Link*) can
  now be set per asset library in the Preferences.
  (blender/blender@ae84a2956e).
  Asset Browsers follow this setting by default, but there's an option
  to override it.  
- *Import Type* has been renamed to *Import Method*.
  (blender/blender@972f58c482)

## glTF 2.0

Find the changelog in the
[Add-ons](add_ons.md) section.
