# Windows System Information 

## Blender 4.3+ - Semi-Automated Information Collection

In Blender 4.3 we added a script that helps automatically collect system information
in the case that Blender isn't opening. The steps to use it are as follows:

- Navigate to where Blender is installed.
  - If you installed Blender through Steam, then you can find where Blender is
  installed by right clicking on Blender in Steam selecting
  `Manage > Browse local files`.
  - If you installed Blender through the Microsoft store, then you can usually
  find Blender at
  `C:\Program Files\WindowsApps\BlenderFoundation.EXTRA_DETAILS\Blender\blender.exe`
- Double click `blender_system_info.cmd` to run the script.
- The script will collect information about your computer and Blender
and open the bug reporting webpage, automatically filling it out with
the information it collected.
- The script is unable to collect information about your graphics card, so you will
need to collect this information manually by following `Graphics Card` section of
the guide below.

---

## Operating System

Click on the search icon on the task bar, then search for and open `dxdiag`.

A DirectX Diagnostic Tool window will open. You will find the operating system
version visible in the `System` tab. Copy it onto your bug report form like this:
`Windows 11 Home 64-bit (10.0, Build 22631)`.

![](./images/windows_get_os_version.jpg)

## Graphics Card

With `dxdiag` open, navigate to the `Display` or `Render` tab and you will find
the `Device name` and `Driver version` of your graphics card.

Combine these details together and share them in your bug report form like this:
`NVIDIA GeForce RTX 4090 - Driver 31.0.15.5186`

![](./images/windows_get_gpu_info.jpg)

## Blender Version

Follow one of the guides below to obtain your Blender version number then
share it on your bug reporting form.

### Downloaded from Blender Website

There are two main ways we recommend finding the Blender version.
Through the command line, or through a GUI.

#### Command line

Open command prompt or terminal and run the command
`/path/to/blender.exe --version`. You will find the Blender version,
commit date, hash, and branch *(Blender 4.2+)*. Combine these details together
and include them on your report form like this:
`Blender 4.2.0, branch: main, commit date: 2024-05-17 00:32, hash: 05617ed07d2b`

![](./images/windows_get_blender_version_command_line.jpg)

#### GUI

Navigate to where Blender is installed, right click on the file `blender.exe`
and select the `Properties` option. In the Properties window that appears,
select the `Details` tab and you will find the Blender version in the field
called `File Version`. Include this on your bug report form like this:
`Blender 4.1.1.0`

![](./images/windows_get_blender_version_gui.jpg)

### Downloaded from Steam

There are two main ways we recommend finding the Blender version.
Through the command line, or through a GUI.

#### Command line

Open Steam, right click on Blender and select the option
`Manage > Browse local files`. A file browser will appear showing where
Blender is installed. Knowing where Blender is now installed, open command
prompt or terminal and run the command `/path/to/blender.exe --version`
You will find the Blender version, commit date, hash, and branch *(Blender 4.2+)*.
Combine these details together and include them on your report form like this:
`Blender 4.2.0, branch: main, commit date: 2024-05-17 00:32, hash: 05617ed07d2b`

![](./images/windows_get_blender_version_steam_command_line.jpg)

#### GUI

Open Steam, right click on Blender and select the option
`Manage > Browse local files`. A file browser will appear showing where
Blender is installed. Right click on `blender.exe` and select
the `Properties` option. In the new window that appears, select the `Details`
tab and you will find the Blender version in the field called `File Version`.
Include this on your bug report form like this: `Blender 4.1.1.0`

![](./images/windows_get_blender_version_steam_gui.jpg)

### Downloaded from Microsoft Store

There are two main ways we recommend collecting Blender version. Through the
command line, or through a GUI.

#### Command line

Open up a command prompt or terminal and run the command
`/path/to/blender.exe --version`. You will find the Blender version,
commit date, hash, and branch *(Blender 4.2+)*. Combine these details together
and include them on your report form like this:
`Blender 4.2.0 - branch: main - commit date: 2024-05-17 00:32 - hash: 05617ed07d2b`

The path to Blender is usually:
`C:\Program Files\WindowsApps\BlenderFoundation.EXTRA_DETAILS\Blender\blender.exe`

![](./images/windows_get_blender_version_command_line.jpg)

#### GUI

Click on the Windows icon on the task bar and find Blender in your apps list.
Once you've found it, right click on Blender and select `More > App Settings`.
The settings app will open and you will find the Blender version in the section
`Specifications` in the field `Version`. Include this on your bug report form
like this: `Blender 4.1.1.0`

![](./images/windows_get_blender_version_microsoft_store.jpg)
