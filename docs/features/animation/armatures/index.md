# Animation & Rigging: Armatures

- [Bone Collections](bone_collections.md)

## To Be Documented

- Skeletal Animation, how pose data is stored, evaluated, and applied.
- Skeleton Editing, how armatures are created and edited,
  including bone naming conventions, mirroring, symmetrizing.
