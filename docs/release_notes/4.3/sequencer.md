# Blender 4.3: Sequencer

## Timeline
- Connect and disconnect strips for easy selection and transformation.
  (blender/blender!124333) <br/>
  ![](images/vse_connected_strips.gif)
- Thumbnails for movie and image strips load faster, and are much less "flickering"
  (e.g. no longer reload on undo).
  For dense timelines, the thumbnail repainting is also several times faster.
  For new files they are enabled by default now.
  (blender/blender!126405, blender/blender!126972, blender/blender!127403)

## Preview
- The preview area now supports snapping.
  Source points include the four corners and origins of all selected, visible strips.
  Snap targets can be the preview boundaries, the preview center,
  or the corners/origins of other strips.
  (blender/blender!122759) <br/>
  ![](images/vse_preview_snap.png)
- Preview area default tool is Box Select now, and toolbars are shown by default.
  (blender/blender!126336)

## User Interface
- Modifiers: Move modifier-specific settings first, Mask settings after. (blender/blender!125710)

## Sound
- It is now possible to adjust Sound strips on a sub-frame level with the "Slip" operator
  (when holding Shift) or directly in the sidebar.
  (blender/blender!123438)

## Performance
- Color Balance modifier is several times faster
  (for regular images) or a bit faster (for float/HDR images).
  (blender/blender!127121)
- Saturation and Multiply strip color controls are several times faster (multi-threaded now).
  (blender/blender!127409)
- Tonemap modifier is 10-15 times faster
  (multi-threaded luminance estimation, color conversion optimizations).
  (blender/blender!127467)
- For images at the bottom channel that use Alpha Over blend mode,
  the actual "blend with transparent black" step can be skipped now.
  This particularly helps float images, e.g. EXR image with RGBA channels at the bottom.
  (blender/blender!127310)
- Various parts of image processing related to color transformations
  (particularly for float/HDR images) are faster now.
  (blender/blender!127305, blender/blender!127307, blender/blender!127308, blender/blender!127346,
  blender/blender!127427)
- Rebuilding movie proxies at lower than 100% resolution is faster,
  by multi-threading the frame downscale step.
  (blender/blender!128054)
- Drawing the timeline is faster for complex timelines:
  - Faster drawing of the channels list. (blender/blender!127913)
  - Faster drawing of waveforms and animation curve overlays.
    (blender/blender!128015)
  - Faster drawing of retiming keys. (blender/blender!128170)
  - Other optimizations. (blender/blender!128057)

