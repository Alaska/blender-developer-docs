# Rocky Linux 8

The official Linux builds are done on Rocky Linux 8.
This makes it possible to have a portable build which will work on
[VFX reference platform](https://vfxplatform.com/) compatible systems.

This page describes how the official setup is created. This setup allows
to compile Blender itself, as well as the Blender dependencies.

## Preparing the System

When provisioning a new system the following details may help:

**Disk Usage**

At the time of writing (Blender 4.2) disk usage is as follows:

- ~2.5 GB: Rocky8 installation.
- ~34 GB: Packages installed by `linux_rocky8_setup.sh`.
- ~42 GB: Blender's GIT repository & result of running `make deps`.
- Some extra space for GIT LFS & building Blender is needed too.

Leaving some extra room to build Blender & run tests, a 100 GB is sufficient.

**System Memory**

The amount of memory needed to build packages depends on the number of simultaneous jobs,
it is recommended no less than 16 GB of memory is used.

For building with 32 jobs requires about 64 GB of memory,
although there are rough numbers to get an idea of what you may need.

**CPU Cores**

On Linux `make deps` builds dependencies one at a time with each build using all available cores.

In general the more cores available, the faster the build will complete.


## Preparing Rocky 8 Environment

The release environment is based on the `Minimal Install` of the Rocky 8.
This document does not cover the steps needed to get the OS installed:
it is rather straightforward and the default installation will get to
where the systems needs to be.

The
[linux_rocky8_setup.sh](https://projects.blender.org/blender/blender/src/branch/main/build_files/build_environment/linux/linux_rocky8_setup.sh)
script is then run as a root on the freshly installed system.
It takes care of installing all dependencies.

!!! Note
    Some system-wide changes are performed, so it is recommended to use a
    dedicated virtual machine for the release build environment.

Congratulations, the base system is now ready!

Follow the [Building Cycles with GPU Binaries](../../building_blender/cycles_gpu_binaries.md)
for the instructions related to installing the GPU compute tool-kits.

## Building Blender

Blender is built from sources using the precompiled Rocky 8 libraries.
Follow the Download Sources and Download Libraries sections from the
[Linux Quick Setup](../../building_blender/linux.md).
Note that all packages have already been installed,
so that step from the Quick Setup should be skipped.
Also make sure to get pre-compiled libraries for Rocky 8.

The build is to be performed using gcc-11 tool-chain. It is activated
using the `scl` command:

``` bash
scl enable gcc-toolset-11 bash
cd ~/blender-git/blender
make release
```
