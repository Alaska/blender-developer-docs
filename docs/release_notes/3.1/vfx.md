# Blender 3.1: VFX & Video

## Sequencer

- Add meta.separate() Python API function
  (blender/blender@9cf3d841a823)
- Support drag and drop for datablocks
  (blender/blender@b42494cf6b00)
- Add drag and drop handler for preview area
  (blender/blender@34370d9fdf33)
- Support copy pasting strips with animation across different scenes.
  Only fcurves are supported.
  (blender/blender@eddad4e9a1ac)
- Use timecodes by default, Proxy size 25% is no longer set by default.
  (blender/blender@eab066cbf2da)
- Automatic proxy building: Build proxies only for slow movies
  (blender/blender@b45e71e22cc7)

## Compositor

- Color space conversion node
  (blender/blender@6beaa297918b)
- Scene time node
  (blender/blender@b2ccd8546c72)
