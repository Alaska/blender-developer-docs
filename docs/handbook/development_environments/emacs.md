# Emacs

## ClangFormat

See [How to clang-format the current buffer on save?](https://emacs.stackexchange.com/questions/48500).

## RST

For editing reStructuredText for the user manual,
the following can be added to your `emacs.d/init.el`.

``` lisp
(add-hook
  'rst-mode-hook
  (lambda ()
    (setq-local fill-column 120)
    (setq-local indent-tabs-mode nil)
    (setq-local tab-width 3)
    (setq-local evil-shift-width 3)

    (add-to-list 'write-file-functions 'delete-trailing-whitespace)

    ;; package: find-file-in-project
    (setq-local ffip-patterns '("*.rst" "*.py"))
    )
  )
```
