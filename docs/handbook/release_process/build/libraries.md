# Library Dependencies

Blender depends on many libraries. The complete list and the versions in
official releases can be found in the Blender repository:

[build_files/build_environment/cmake/versions.cmake](https://projects.blender.org/blender/blender/src/branch/main/build_files/build_environment/cmake/versions.cmake)

## Precompiled Libraries

Precompiled libraries are available for Windows, macOS and Linux in an
SVN repository. These are the easiest and most reliable way to get a
feature complete Blender build. Blender builds with these libraries are
portable and can be run on other computers.

For Windows and macOS, precompiled libraries are automatically
downloaded when running `make update`. Building library dependencies on
these platforms is normally only done by platform maintainers.

For Linux, precompiled libraries must be downloaded once manually as per
the build instructions, and can then be updated with `make update`.

## make deps

The Blender repository has a CMake based system to build all library
dependencies from source. This is the system used to generate
precompiled libraries for Windows, macOS and Linux.

The `make deps` command performs the cmake setup and build. It is mainly
used by platform maintainers and is not tested to work on many different
operating system installs. A successful build may require some manual
configuration or edits to the code. Minimal instructions on how to use
it can be found in
[build_files/build_environment/CMakeLists.txt](https://projects.blender.org/blender/blender/src/branch/main/build_files/build_environment/CMakeLists.txt).

## System Packages

On Linux, the system package manager can also be used to install many
dependencies. See the Linux build instructions for details.

The resulting builds will not be portable, but libraries can be shared
with other software on the system.

The simplest way to install all needed packages is to run (from the
blender source directory):

` build_files/build_environment/install_linux_packages.py --all`
