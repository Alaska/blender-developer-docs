# Grease Pencil migration from older versions to Blender 4.3

With Grease Pencil completely rewritten in 4.3, it is expected that some features work differently.
This page explains the migration process for users as well as add-on developers.

## Python API

Add-ons written using the Grease Pencil python API before 4.3
are expected to break in this version.
This section is meant to help add-on developers update their add-ons for Blender 4.3.

### Annotations

Annotations still use the legacy Grease Pencil ID type to store their data.
For compatibilty reasons, the RNA types for annotations become the legacy Grease Pencil RNA types:

- `bpy.types.GreasePencil`: Annotation data-block.
- `bpy.types.GreasePencilLayers`: Annotation layer collection.
- `bpy.types.GPencilLayer`: Annotation layer.
- `bpy.types.GPencilFrames`: Annotation frames collection.
- `bpy.types.GPencilFrame`: Annotation frame.
- `bpy.types.GPencilStroke`: Annotation stroke.
- `bpy.types.GPencilStrokePoint`: Annotation stroke point.

This is because annotations use the same data-structure as the legacy Grease Pencil.

The API for annotations has been reduced to a minimum.
Users and add-on developers are encouraged to use Grease Pencil instead of annotations for more
complicated tasks.

Here is a description of the types and their properties
(see the [Blender Python API
documentation](https://docs.blender.org/api/current/bpy.types.GreasePencil.html)
for a full description):

- `bpy.types.GreasePencil`: Annotation data-block.
  - `animation_data` (`bpy.types.AnimData`):  Animation data for this data-block.
  - `layers` (`GreasePencilLayers` of `GPencilLayer`): Collection of annotation layers.
- `bpy.types.GreasePencilLayers`: Annotation layer collection.
  - `active_index` (`int`): Index of active annotation layer.
  - `active_note` (`GPencilLayer`): Note/Layer to add annotation strokes to.
  - `new(:name, :set_active?)`: Function to add a new annotation layer.
  - `remove(:layer)`: Function to remove an annotation layer.
- `bpy.types.GPencilLayer`: Annotation layer.
  - `active_frame` (`GPencilFrame`): Frame currently being displayed for this layer
  - `annotation_hide` (`bool`): Set annotation Visibility.
  - `annotation_onion_after_color` (`mathutils.Color`):
    Base color for ghosts after the active frame.
  - `annotation_onion_after_range` (`int`):
    Maximum number of frames to show after current frame.
  - `annotation_onion_before_color` (`mathutils.Color`):
    Base color for ghosts before the active frame.
  - `annotation_onion_before_range` (`int`):
    Maximum number of frames to show before current frame.
  - `annotation_onion_use_custom_color` (`bool`):
    Use custom colors for onion skinning instead of the theme.
  - `annotation_opacity` (`float`): Annotation Layer Opacity.
  - `color` (`mathutils.Color`): Color for all strokes in this layer.
  - `frames` (collection of `GPencilFrame`): Sketches for this layer on different frames.
  - `info` (`str`): Layer name.
  - `is_ruler` (`bool`): This is a special ruler layer.
  - `lock` (`bool`): Protect layer from further editing and/or frame changes.
  - `lock_frame` (`bool`): Lock current frame displayed by layer.
  - `select` (`bool`): Layer is selected for editing in the Dope Sheet.
  - `show_in_front` (`bool`): Make the layer display in front of objects.
  - `thickness` (`int`): Thickness of annotation strokes.
  - `use_annotation_onion_skinning` (`bool`):
    Display annotation onion skins before and after the current frame
- `bpy.types.GPencilFrames`: Annotation frames collection.
  - `new(:frame_number, :active?)`: Function to add a new annotation frame.
  - `remove(:frame)`: Function to remove an annotation frame.
  - `copy(:source)`: Function to copy the source frame.
- `bpy.types.GPencilFrame`: Annotation frame.
  - `frame_number` (`int`): The frame on which this sketch appears.
  - `select` (`bool`): Frame is selected for editing in the Dope Sheet.
  - `strokes` (collection of `GPencilStroke`): Freehand curves defining the sketch on this frame.
- `bpy.types.GPencilStroke`: Annotation stroke.
  - `points` (collection of `GPencilStrokePoint`): Stroke data points.
- `bpy.types.GPencilStrokePoint`: Annotation stroke point.
  - `co` (`mathutils.Vector`): Coordinates of this point.

### Grease Pencil Object and ID

|                   | Blender 4.2              | Blender 4.3                   |
| ----------------- | ------------------------ | ----------------------------- |
| Object type       | `GPENCIL`                | `GREASEPENCIL`                |
| RNA ID type       | `bpy.types.GreasePencil` | `bpy.types.GreasePencilv3`    |

### Grease Pencil Data
*TODO*

### Modifiers

Grease Pencil modifiers are now regular modifiers. 

|                   | Blender 4.2                      | Blender 4.3                   |
| ----------------- | -------------------------------- | ----------------------------- |
| Modifiers         | `object.grease_pencil_modifiers` | `object.modifiers`            |
| Modifier ID type  | `bpy.types.GpencilModifier`      | `bpy.types.Modifier`          |

Grease Pencil modifier property name changes

|                                  | Blender 4.2          | Blender 4.3                |
| -------------------------------- | -------------------- | -------------------------- |
| Opacity Modifier -> Factor       | `factor`             | `color_factor`             |

### Operators
*TODO*
