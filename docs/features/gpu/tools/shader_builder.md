# ShaderBuilder

Using the CMAKE option `WITH_GPU_BUILDTIME_SHADER_BUILDER=On` will precompile each shader to
make sure that the syntax is correct and that all the generated code compiles and
links with the main shader code.

Only shaders that are part of the `SHADER_CREATE_INFOS` and `.do_static_compilation(true)`
is set, will be compiled. Enabling this option would reduce compile roundtrips when
developing shaders as during compile time the shaders are validated, compiled and linked
on the used platform.

By default shader builder checks against all GPU backends that can run on your system.

## Command line arguments

Shader builder has several command line argument to be selective what will be tested.

`--gpu-backend` will only test one specific backend. Possible options are:

* `metal`
* `opengl`
* `vulkan`

> NOTE: Not all options are available on all platforms.

`--gpu-shader-filter` can be use to test a subset of shaders. It only
test shaders that have a name that starts with the next given argument.

* `--gpu-shader-filter eevee` would test all EEVEE shaders.
* `--gpu-shader-filter compositor` would test compositor shaders.
* `--gpu-shader-filter eevee_shadow_tag_usage_opaque` would only test that specific shader.

> NOTE: The filter option is useful when developing backend features. Often debug code was added
> to focus on a single shader at a time. 
