# Compatibility Changes

Some of the changes in Blender significantly break [backward compatibility](https://en.wikipedia.org/wiki/Backward_compatibility) or [forward compatibility](https://en.wikipedia.org/wiki/Forward_compatibility).

Here you can find the major breaking changes introduced by the recent releases of Blender, as well as the ones planned for future releases. These are collected from the individual [release notes](../index.md) and [design tasks](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=301&milestone=0&project=0&assignee=0&poster=0).


## Upcoming Releases


??? example "[Blender 5.0](https://projects.blender.org/blender/blender/issues?type=all&sort=&state=open&labels=&milestone=20&project=0&assignee=0&poster=0) — Nov 2025"

    ## [Remove Big-Endian Support](https://projects.blender.org/blender/blender/issues/125759)
    
    This type of architectures are virtually inexistent nowadays on Blender hardware targets,
    they have not been officially supported for years, and the current code is essentially
    fully untested on these.

    ## API-related Changes

    ### [IDProperties](https://projects.blender.org/blender/blender/issues/123232)

    Several changes are proposed to the IDProperties/Custom Data system, which would largely affect their usages through the API (make all IDProperty statically typed, separate user-defined and system-defined IDProperties).

    ### [UI API](https://projects.blender.org/blender/blender/issues/110461)

    In particular, several features from the `UIList` may be removed.


??? example "Blender 4.5 LTS — Jul 2025"

    There are no significant compatibility changes planned yet for this release.


??? example "Blender 4.4 — Mar 2025"

    There are no significant compatibility changes planned yet for this release.


???+ tip "[Blender 4.3](../4.3/index.md) — Nov 2024 (alpha)"

    ### [Cycles](../4.3/cycles.md)
    
    * Support for AMD and Intel GPUs through the Metal backend on macOS has been removed.
    * The minimum driver version requirement for NVidia GPUs has been increased to 495.89

    ### [Grease Pencil](../4.3/grease_pencil.md)

    The Grease Pencil type of object will be rewritten to allow deeper changes, removing long standing limitations and facilitating future evolutions. The new data model will be fully incompatible with previous Blender releases.
    
    ### [Brush Assets](../4.3/sculpt.md#brushes)
    
    The brush workflow has been redesigned to use global brush asset libraries. Brushes from previous versions [need conversion](../4.3/sculpt.md#converting-brushes).

    ### [Python API](../4.3/python_api.md#breaking-changes)

    The `AttributeGroup` type was split into one new type per attribute owner.


## Published Releases


???+ success "[Blender 4.2 LTS](../4.2/index.md#compatibility) — Jul 2024 - Jul 2026"

    ### [EEVEE](../4.2/eevee.md)

    The EEVEE render engine was completely rewritten. Read about the [Migration Process](https://developer.blender.org/docs/release_notes/4.2/eevee_migration/) from previous versions. Some platforms and hardware are not working as expected yet, see the [Platform Compatibility](https://developer.blender.org/docs/release_notes/4.2/eevee/#platform-compatibility) section for details.

    ### [Compositor](../4.2/compositor.md)

    The Compositor now supports GPU rendering. Due to this, a number of changes were done to the way the canvas is computed, and some nodes have been adapted to support both CPU and GPU. See the [list of breaking changes](https://developer.blender.org/docs/release_notes/4.2/compositor/#breaking-changes).

    ### [Add-ons and Themes](../4.2/extensions.md)

    Most add-ons that used to ship with Blender, are now available on the [Extensions Platform](https://extensions.blender.org/) where you can browse, install, and update them online from within Blender.

    ### [SSE4.2 CPU](https://devtalk.blender.org/t/proposal-bump-minimum-cpu-requirements-for-blender/26855)

    On Windows and Linux a CPU with SSE4.2 is now required.
    This is supported since AMD Bulldozer (2011) and Intel Nehalem (2008).

    ### [Statically Typed IDProperties](../4.2/python_api.md#statically-typed-idproperties)

    System-defined and library-overridable IDProperties are now statically typed.


??? boringbox-check "[Blender 4.1](../4.1/index.md#compatibility) — Mar 2024"

    ### [Auto Smooth](https://projects.blender.org/blender/blender/issues/93551)

    The "Auto Smooth" option has been replaced by a modifier node group asset.

    ### [Apple](https://projects.blender.org/blender/blender/commit/17ca22ae)

    macOS 11.2 (Big Sur) is now the minimum required version for Apple computers.
    This is required for the Metal backend to work correctly.

    ### [Libraries](https://code.blender.org/2022/09/vfx-reference-platform-2023-2024/)

    Libraries have been upgraded to match [VFX platform 2024](https://vfxplatform.com/).


??? boringbox-check "[Blender 4.0](../4.0/index.md#compatibility) — Nov 2023"

    ### [Blend Files](../4.0/core.md#blend-file-compatibility)

    The mesh format changes from previous versions are now included in the Blender file format. Blender 3.6 LTS can read files saved with 4.0, and will save meshes in a format compatible with older versions of Blender.

    ### [Unused Linked Data](../4.0/core.md#breaking-changes)
    Unused linked data is not kept anymore when saving and re-opening .blend files.

    ### [Graphics Cards](../4.0/index.md#graphics-cards)

    The minimum required OpenGL version has been increased to 4.3 for Linux and Windows. On macOS only Metal is supported now.

    Support for Intel HD4000 series GPUs has been dropped.

    ### [Bone Collections](../4.0/animation_rigging.md#bone-collections-and-colors)

    New bone collections replace both legacy numbered layers and bone groups.


??? success "[Blender 3.6 LTS](../3.6/index.md#compatibility) — Jun 2023 - Jun 2025"

    There are no significant compatibility changes in this release.


??? boringbox-check "Archived Releases — 3.3 - 3.5"

    ??? boringbox-check "[Blender 3.5](../3.5/index.md#compatibility) — Mar 2023"

        ### New Minimum Requirements

        - **macOS**
          - **macOS 10.15** (Catalina) for **Intel** devices
          - macOS 11.0 (Big Sur) remain the minimum for for Apple Silicon.
        - **Linux** distribution with **glibc 2.28**, including:
          - **Ubuntu 18.10**
          - **Debian 10** (Buster)
          - **Fedora 29**
          - **RHEL 8** and derivatives CentOS, Rocky Linux and AlmaLinux

        ### [Libraries](https://code.blender.org/2022/09/vfx-reference-platform-2023-2024/)

        Libraries have been upgraded to match [VFX platform 2023](https://vfxplatform.com/).



    ??? boringbox-check "[Blender 3.4](../3.4/index.md#compatibility) — Dec 2022"

        ### [Nodes](../3.4/nodes_physics.md)

        The `Transfer Attribute` node has been split into three separate nodes.

        The `MixRGB` nodes for Shader and Geometry Nodes are converted to the new "Mix Node" on loading. Files saved with the new node are not forward compatible with older versions of Blender.

        Because of asset support in node editor 'Add' menus, node group asset files must be re-saved in version 3.4.


    ??? boringbox-check "[Blender 3.3 LTS](../3.3/index.md) — Sep 2022 - Sep 2024"

        There are no significant compatibility changes in this release.
