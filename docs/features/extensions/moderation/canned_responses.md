# Canned Responses

These are to be used as a start point, combined with some extra explanations if needed.

!!! NOTE
    These responses are to ensure extensions adhere to:

    - [Terms of Service](https://extensions.blender.org/terms-of-service/)
    - [Add-on Guidelines](../../../handbook/addons/guidelines/index.md)

## General

### Low Quality

This extension does not seem to provide enough value for users for it to be included.

```
This extension does not seem to provide enough value for users for it to be included.
```

### Broken Add-on

The add-on is not working at the moment, please
[Install from Disk](https://docs.blender.org/manual/en/dev/editors/preferences/extensions.html)
to test if everything is working well.

```
The add-on is not working at the moment, please
[Install from Disk](https://docs.blender.org/manual/en/dev/editors/preferences/extensions.html)
to test if everything is working well.
```

### Includes an Updater

Add-ons should not be including their own updating functionality as this is is handled by
Blender directly now.

```
Add-ons should not be including their own updating functionality as this is is handled by
Blender directly now.
```

### Uses an Updater Without Consent

This add-on makes use of an updater which connects to the internet without user consent
(or setting the corresponding `permission`).

```
This add-on makes use of an updater which connects to the internet without user consent
(or setting the corresponding `permission`).
```

### Request External Components

Extensions should not require any external functional components (other than Blender itself)
that need to be downloaded from elsewhere.
See the Extensions Platform [Terms of Service](https://extensions.blender.org/terms-of-service/).

If you can't (or don't want) to change your extension to adapt to it, you can
[self-host](https://docs.blender.org/manual/en/dev/extensions/getting_started.html#third-party-extension-sites)
it without this restriction.

```
Extensions should not require any external functional components (other than Blender itself)
that need to be downloaded from elsewhere.
See the Extensions Platform [Terms of Service](https://extensions.blender.org/terms-of-service/).

If you can't (or don't want) to change your extension to adapt to it, you can
[self-host](https://docs.blender.org/manual/en/dev/extensions/getting_started.html#third-party-extension-sites)
it without this restriction.
```

## Code Quality

### Includes Python's Cache

This extension bundles byte-code compiled code `__pycache__`,
which could differ from the original code in ways we can't easily validate and isn't necessary.

This should be removed, we recommend to use the extension `build` command
which wont include these files. e.g. `blender -c extension build`.
see:
[Extension Command Line Arguments
](https://docs.blender.org/manual/en/dev/advanced/command_line/extension_arguments.html#command-line-args-extension-build)

```
This extension bundles byte-code compiled code `__pycache__`,
which could differ from the original code in ways we can't easily validate and isn't necessary.

This should be removed, we recommend to use the extension `build` command
which wont include these files. e.g. `blender -c extension build`.
see:
[Extension Command Line Arguments
](https://docs.blender.org/manual/en/dev/advanced/command_line/extension_arguments.html#command-line-args-extension-build)
```

### Backslash Path Separators

This add-on uses backslash literals as path separators which won't work on Linux or macOS.

Use `os.sep` for the native path separator or utility functions such as `os.path.join`
or the `pathlib` module.

```
This add-on uses backslash literals as path separators which won't work on Linux or macOS.

Use `os.sep` for the native path separator or utility functions such as `os.path.join`
or the `pathlib` module.
```

### Absolute MS-Windows Paths

This add-on uses absolute MS-Windows paths (including drive letters such as `C:\`).

Add-ons should query the system using relevant API's
instead of assuming absolute paths exist on the users system.

```
This add-on uses absolute MS-Windows paths (including drive letters such as `C:\`).

Add-ons should query the system using relevant API's
instead of assuming absolute paths exist on the users system.
```

### Hard Coded Data-Block Names

This add-on makes error prone assumptions about data block names in the users file:
e.g. `bpy.data.worlds["World"]`

```
This add-on makes error prone assumptions about data block names in the users file:
e.g. `bpy.data.worlds["World"]`
```

### Uses `__name__` ID for Preferences

Using `__name__` to lookup add-on preferences is no longer valid.
See how to [use `__package__`
](https://docs.blender.org/manual/en/dev/advanced/extensions/addons.html#user-preferences-and-package)
instead.

```
Using `__name__` to lookup add-on preferences is no longer valid.
See how to [use `__package__`
](https://docs.blender.org/manual/en/dev/advanced/extensions/addons.html#user-preferences-and-package)
instead.
```

### Manipulates Python's Module Loading

This add-on manipulates Python's module search to expose modules:
Bundled modules must either be:

* Loaded as sub-modules, or in the case of modules from the
  [Python Package Index (pypi.org)](https://pypi.org).
* Included as
  [Python Wheels](https://docs.blender.org/manual/en/latest/advanced/extensions/python_wheels.html)
  (see the [Guidelines](https://developer.blender.org/docs/handbook/addons/guidelines/)).

```
This add-on manipulates Python's module search to expose modules:
Bundled modules must either be:

* Loaded as sub-modules, or in the case of modules from the
  [Python Package Index (pypi.org)](https://pypi.org).
* Included as
  [Python Wheels](https://docs.blender.org/manual/en/latest/advanced/extensions/python_wheels.html)
  (see the [Guidelines](https://developer.blender.org/docs/handbook/addons/guidelines/)).
```

### Assumes Add-ons Directory is Writable

This add-on assumes the add-ons directory is writable (which may not be the case),
use `bpy.utils.extension_path_user` to create a writable path for the extension.

See the [Guidelines](https://developer.blender.org/docs/handbook/addons/guidelines/) and
[Local Storage](https://docs.blender.org/manual/en/latest/advanced/extensions/addons.html#local-storage).

```
This add-on assumes the add-ons directory is writable (which may not be the case),
use `bpy.utils.extension_path_user` to create a writable path for the extension.

See the [Guidelines](https://developer.blender.org/docs/handbook/addons/guidelines/) and
[Local Storage](https://docs.blender.org/manual/en/latest/advanced/extensions/addons.html#local-storage).
```

### Insecure use of `eval()` / `exec()`

This add-on uses `eval()` / `exec()` which are potentially insecure
when input is not completely under the developers control
(when values from a blend-file are included for example).

Where possible, these functions should be avoided:

- In many cases `getattr(data, attr)` & `setattr(data, attr, value)`
  can be used as an alternative.
- In other cases this is used to load data from a file.
  Alternatives such as Python's `pickle` module or JSON should be used instead.
- When a Python literal needs to be parse from an untrusted source
  consider using consider using the `ast` module (`ast.pare` & `ast.literal_eval`).

```
This add-on uses `eval()` / `exec()` which are potentially insecure
when input is not completely under the developers control
(when values from a blend-file are included for example).

Where possible, these functions should be avoided:

- In many cases `getattr(data, attr)` & `setattr(data, attr, value)`
  can be used as an alternative.
- In other cases this is used to load data from a file.
  Alternatives such as Python's `pickle` module or JSON should be used instead.
- When a Python literal needs to be parse from an untrusted source
  consider using consider using the `ast` module (`ast.pare` & `ast.literal_eval`).
```

### Strings Are Not Escaped

This add-on seems to use names in literal quotes without being escaped.
Names may themselves contain quotes or other characters that need to be escaped.

For example:
``data_path = 'objects["{:s}"]'.format(object.name)``
is incorrect because ``object.name`` may contain a ``"`` character.

To resolve:
- For Python strings, use `repr(...)`
  or the `!r` conversion flag for f-strings or `str.format`.
- For Blender's data-paths (strings used for F-curves data-paths for example),
  use `bpy.utils.escape_identifier(..)`.

```
This add-on seems to use names in literal quotes without being escaped.
Names may themselves contain quotes or other characters that need to be escaped.

For example:
``data_path = 'objects["{:s}"]'.format(object.name)``
is incorrect because ``object.name`` may contain a ``"`` character.

To resolve:
- For Python strings, use `repr(...)`
  or the `!r` conversion flag for f-strings or `str.format`.
- For Blender's data-paths (strings used for F-curves data-paths for example),
  use `bpy.utils.escape_identifier(..)`.
```


### Strings Strip Characters in Any Order

This add-on uses `str.strip`, `str.lstrip` or `str.rstrip`,
taking multiple characters that seem to assume order.

All characters are stripped in any order.

So `"test_nojs.json".strip(".json")` results in `"test_"`, not `test_nojs`.

Use `str.removeprefix` & `str.removesuffix` instead.

```
This add-on uses `str.strip`, `str.lstrip` or `str.rstrip`,
taking multiple characters that seem to assume order.

All characters are stripped in any order.

So `"test_nojs.json".strip(".json")` results in `"test_"`, not `test_nojs`.

Use `str.removeprefix` & `str.removesuffix` instead.
```

### Operating on all Objects

This add-on operates on all `bpy.data.objects`,
meaning it will operate on objects in other scenes.

Unless there are reasons to do otherwise `context.scene.objects` should be used
to prevent tools from operating on data in scenes which aren't currently visible.

```
This add-on operates on all `bpy.data.objects`,
meaning it will operate on objects in other scenes.

Unless there are reasons to do otherwise `context.scene.objects` should be used
to prevent tools from operating on data in scenes which aren't currently visible.
```

### Manifest Should Have `platforms` (Has Binary Files)

Binary files are included but the manifest doesn't define supported `platforms`.

```
Binary files are included but the manifest doesn't define supported `platforms`.
```

### Manifest Shouldn't Have `platforms` (No Binary Files)

The `blender_manifest.toml` defines `platforms`
even though the extension doesn't seem to include any platform specific code.

This will make the extension incompatible with other platforms & architectures.

Unless platforms is needed to exclude incompatible platforms, it should be removed.

```
The `blender_manifest.toml` defines `platforms`
even though the extension doesn't seem to include any platform specific code.

This will make the extension incompatible with other platforms & architectures.

Unless platforms is needed to exclude incompatible platforms, it should be removed.
```

### Obfuscated Code

This extension violates the [Terms of Service](https://extensions.blender.org/terms-of-service/):
> no obfuscated code or byte code is allowed.

Example: `split_3D044 = split_17838.split(factor=0.5, align=True)`

```
This extension violates the [Terms of Service](https://extensions.blender.org/terms-of-service/):
> no obfuscated code or byte code is allowed.

Example: `split_3D044 = split_17838.split(factor=0.5, align=True)`
```
