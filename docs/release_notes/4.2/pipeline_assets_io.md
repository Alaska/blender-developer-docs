# Blender 4.2 LTS: Import & Export

## Collection Exporters
File format exporters can now be associated with Collections. One or
more exporters can be added and configured in the Collection properties
panel. Settings are stored in .blend files for easy sharing and persistence
across Blender sessions. (blender/blender@509a7870c3570cbf8496bcee0c94cdc1e9e41df5)

This feature streamlines the process of re-exporting the same asset(s)
repeatedly. For example when creating glTF assets for a game and
iterating on the look, or when using Blender in a studio pipeline to
create USD assets.

All collections in the scene can be exported with a single
click through the File menu. Additionally, each collection can be
exported to multiple file formats simultaneously.

![](./images/io_collection_export.png){style="width:400px;"}

## USD
- The hair curves object type is now supported for both import and export. (blender/blender@ea256346a89, blender/blender@21db0daa4e5)
- Point cloud import is now supported. (blender/blender@e4ef0f6ff4fe1df8188a85e2a6b33e62801085e3)
- Unicode names are now supported on import and are optional for export. Only software using USD 24.03 or greater can support Unicode files. (blender/blender@9ad2c7df0b8f)
- New Import options
  - Import only defined prims or all (blender/blender@bfa54f22ceb)
  - Convert USD dome light to a world shader (blender/blender@e1a6749b3d9)
  - Validate meshes on import (blender/blender@2548132e23f)
- New Export options
  - Filter which types to export (blender/blender@a6a5fd053a3cd036ac06028dc11c5b4c37e7935a)
  - Convert the world shader to a USD dome light (blender/blender@e1a6749b3d9)
  - Stage up axis (blender/blender@24153800618)
  - XForm operator convention (blender/blender@36f1a4f94f3)
  - Triangulate meshes (blender/blender@b2d1979882f)
  - Down-sample exported textures for USDZ (blender/blender@3e73b9caa5a)
  - Generate MaterialX network from Blender shader nodes. Supports a subset of all shader nodes and their functionality. (blender/blender!122575)
  - Rename active UV map to `st`, following USD conventions. This is enabled by default. (blender/blender@eaeb8ba8cd5a00396ab2d4fdb448b4f7cd127bb6)

## Alembic
- The hair curves object type is now supported for both import and export. (blender/blender@ea256346a89, blender/blender@b24ac18130e)
- Import multiple files at once. (blender/blender!121492)
- Fix: Addressed long-standing issue where animated curves would not update during render. (blender/blender@ea256346a89)

## glTF

glTF is now part of the core add-ons bundle that ships with Blender. (blender/blender@13a3603578f87ee47eb69e26271dc67d2d7df209)

- Importer
  - Option to set bone shape + bone shape size (blender/blender@08d43667c82de0f8cdb002950e1e91269a3478e4)
  - EEVEE : Use nodes for alpha clipping (blender/blender@b583b6ec39ed1082d871f105a897992de911cd89)
  - Remove Eevee legacy blend_method (blender/blender@d9d5597a6c850810f8f9998007abb9e4290323aa)
  - Implement KHR_animation_pointer (blender/blender-addons@f8368df0c8c1f20c1df7394a1f4e82a6e3620c70)
  - Use layout panels instead of PanelTypes (for collection export) (blender/blender-addons@033d552a7baed73762db64778dd5e444bc5ed51b)
- Exporter
  - New features
    - Option to export Vertex Color even if not use in material node tree (blender/blender@0f0a8df8a922cdb0796c0b3fdbdc5c669efb3059)
    - Implement KHR_animation_pointer (blender/blender-addons@f8368df0c8c1f20c1df7394a1f4e82a6e3620c70)
    - Enable collection export (blender/blender-addons@bf2d56e814b1b59f52998b3d134cf66a13ede35d)
    - Collection export option to export at center of mass of root objects (blender/blender@c6e38d084ce14604d1781b667c167ca279c5e5e1)
    - Perf: on 'disable viewport' option, manage drivers too (blender/blender@6a4210ff90af3bf1f5f928223b85babd69c24732)
    - Add profile option (debug) at export (blender/blender-addons@804a2766880f281fd8dd1c6b08dd44b9b497912f)
    - EEVEE : Use nodes for alpha clipping (blender/blender@b583b6ec39ed1082d871f105a897992de911cd89)
    - EEVEE : backwards compatibility for alpha clipping (blender/blender@8fd9712df93f2a56b5dc0c4709afd187549800b6)
    - UDIM for merged channel texture (blender/blender@a71027d37f554284776a2f6f7c87b4c9ecbffb6d)
    - Allow float socket (such as alpha) coming from a color socket (blender/blender@c05c08cbffe49ad5180aba17cc1189130da56a2c)
  - Fixes
    - Fix alpha Vertex Color export (blender/blender-addons@730987cce3254877de2ee96736e5b781d64ef96d)
    - Fix UVMAP export when coming from an attribute node (blender/blender@ac979078417e7093e1088b05e0065337d73816b3)
    - Fix exporting both factor & Vertex Color at same time (blender/blender@d80a1dd5f80e5c017392c4e5e5fbe3d71687bf45)
    - Do not set/reset object when no track to export (blender/blender-addons@b156daa989ff88f2705219a7dfe4139c3f9bde1a)
    - Fix (experimental) Geometry Nodes export when modifiers + shapekeys (blender/blender-addons@786428011f9a5aa164dfdc981db9bc0c81dab8a1)
    - Fix crash exporting instanced collection whamp light option is enabled (blender/blender-addons@6b8b29648c3bb05f20d99f6eca775bffd05d490a)
    - Avoid crash with full hierarchy + light export (blender/blender-addons@6ac680c1a7f7177f12ba0e6c67426d5151d029e0)
    - Fix crash filtering collections (blender/blender-addons@c794324c8a6353823440969fdf40fe7c069bdd49)
    - Instanced collections can have regular children (blender/blender@be96d5144add6723d6caa77b0d6b601a50035196)
    - Fix material on evaluated data (blender/blender@c297a9513ef376770aa3db728f5a3eaba2b410fa)
    - Fix hierarchy of objects inside full hierarchy of collections (blender/blender-addons@68c49290e31074a6e352c3ff73700ad465b344bc)
    - Fix Geometry Nodes data check (blender/blender@9ba22d4bc780f084ee78527cec0d41277b7cd082)
    - Remove some false positive user warnings (blender/blender@cab79018eea238c5a8d96567c9ec2a1a05cb354f)
    - Fix vendor extension declaration (blender/blender@04b08bcb40e03ad931aa3b8b5dd393fcfda9a711)
    - Various typo fixes (blender/blender-addons@d41feda6c723891a745b127d7f7eecfef22ad7d4, blender/blender-addons@f5f6c460ed5502c70acb7dce637c80b86fad09ec)
    - Fix crash on unlit + animation pointer (blender/blender-addons@1c840422cf65ba4e000cd81fbdcb444d19d2672f)
    - Cleanup user warnings (blender/blender@f8b8a1a5b6066b898f2fb831155a5be175180372)
    - Update hook UI registration after Blender changes (blender/blender@018ece530931dda3dba01b0e90a6d307e657befc)
    - Various fixes:
      - blender/blender@5789031072269b7c7af9576221b3363107373b56
      - blender/blender@c9daea2d5b5213c68e6d2d706ebeaed28bd62f6b
      - blender/blender@f86fe707cecc0f716643181d9351b4cf07c70faf

## COLLADA
The built-in support for COLLADA is now [considered legacy](https://devtalk.blender.org/t/moving-collada-i-o-to-legacy-status/34621). It will be removed in a future version. (blender/blender@82f9501a4a, blender/blender-manual@20e4984ad6)

## Validation

OBJ, PLY, STL importers now default to the Validate Meshes option being on.
This makes import process a bit slower, but prevents later crashes in case input files
contain malformed data. Validation itself is now 2-3x faster compared to previous versions.
(blender/blender@761dd6c9238, blender/blender!121413).
