# Blender 4.3 Release Notes

Blender 4.3 is currently in **Alpha** until October 2,
2024.
[See schedule](https://projects.blender.org/blender/blender/milestone/21).

Under development in [`main`](https://projects.blender.org/blender/blender/src/branch/main).

* [Animation & Rigging](animation_rigging.md)
* [Compositor](compositor.md)
* [Core](core.md)
* [Cycles](cycles.md)
* [EEVEE & Viewport](eevee.md)
* [Extensions](extensions.md)
* [Geometry Nodes](geometry_nodes.md)
* [Grease Pencil](grease_pencil.md)
* [Modeling & UV](modeling.md)
* [Pipeline, Assets & I/O](pipeline_assets_io.md)
* [Python API & Text Editor](python_api.md)
* [Rendering](rendering.md)
* [Sculpt, Paint, Texture](sculpt.md)
* [User Interface](user_interface.md)
* [Video Sequencer](sequencer.md)

## Compatibility
### Cycles

* On macOS, support for AMD and Intel GPUs through the Metal backend of Cycles has been removed
  (blender/blender@c8340cf7541515a17995c30b4a236ac2a326f670)
* The minimum driver version requirement for NVIDIA GPUs has been increased to 495.89
  as a result of updating to OptiX 7.4
  (blender/blender@4f5b6b114d7aaf6b7638d6e05e82b1faf47413f0)

[See all compatibility changes](../../release_notes/compatibility/index.md)
