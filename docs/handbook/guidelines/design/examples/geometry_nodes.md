# Geometry Nodes

Geometry Nodes was a project which started officially on October 19th, 2020.
This document covers its design process and dynamics, with emphasis on when this was still a project before moving to module work.

Topics:

* [Journey](#journey)
* [Caveat](#caveat)
* [Team](#team)
  * [Dynamic](#dynamic)
  * [Product ownership](#product-ownership)
* [Links](#links)
  * [Design presentations](#design-presentations)
  * [Design proposals](#design-proposals)
  * [Workshop and posts with design snippets](#workshop-and-posts-with-design-snippets)

## Journey

These steps roughly represent the order on which the design and development process was carried out.

* **Audience**
  * The project started by picking real artists (as oppose to personas) and connecting to them.
  * Andy Goralczyk (art director / lead VFX).
* **Discovery**
  * Evaluated what was already possible or planned for the ongoing particle-nodes project.
  * Comb over previous open movie projects, which ones would benefit from a new particle system.
  * Interview with Andy: Present him all the possibilities, get back that all he was really looking for were good, predictable set dressing tools.
* **Scope**
  * Decision to pivot to set dressing.
  * Analyze upcoming movie project (Sprite Fright) to see which shots could benefit from Geometry Nodes.
  * List these as potential use-cases.
* **Design**
  * Discussion with stakeholders to agree on design as well as implementation approach.
  * At this point, the concept of "Geometry Nodes" as a modifier was consolidated.
* **Planning**
  * Come up with use cases that make sense in terms of incremental complexity.
  * Dissect the initial use cases - how to make them? Which nodes are required.
  * Build real production files in preparation for the first use-case.
* **Prototyping**
  * This project was a little different since there were a lot of technical solutions already implemented under the hood (due to the prior particle-nodes experiments).
  * If that was not the case, it would have been reasonable to first do a prototype for the simplest use cases before building at scale.
  * Nonetheless, the initial deliverables could have been considered spikes (development to learn, to be trashed) - more on that in validation.
  * As a rule of thumb, you want to get unknowns out of the way, and in Blender, this is often new design, big intended performance gains, or depending/navigating old code (technical debt).
* **Validation**
  * Before proceeding, the design should have been more widely presented (to stakeholders - design leads) before continuing.
  * This would allow iteration over the ideas, to see if the intended outcomes were more concrete.
  * Although things were presented and approved to continue to the next milestones, there was a blind spot when it came to the so-called level of abstraction the system should have supported from the get-go ([*](#caveat)).
* **Delivery**
  * The project was designed to be releasable at every step of the way.
  * This is deeply rooted in Blender's motto of: release value often, document what works, document what is not targeted yet.
  * For Geometry Nodes, this meant that the moment only a few nodes were in (enough to scatter some pebbles), the system was considered ready for a release - even if it meant it couldn't yet do plenty of other use cases.

## Caveat

For a more accurate account of events, watch the [2022 Blender Conference presentation](https://www.youtube.com/watch?v=nmGs9fEPcrA&list=PLa1F2ddGya__2Ip2UB-xp6M54cQVvw1W-) and how the team had to backtrack on the attributes design.

Basically, the expectation was that hard-coded values should not have been supported as the primary way to access attributes. Instead, geometry nodes systems should be fully encapsulatable, with all its dependencies coming from "outside". This was only course-corrected after two Blender releases leading to a cost (no-backward compatibility, outdated documentation, community trust).

This led to what became the Fields design, not without great breakage. Some of this could have been avoided had the PM made sure the stakeholders
were actually aware of what was being built - e.g., present in some of the product demos.

---

Since the topic at hand is design, it is important to also acknowledge how the design competence will be fulfilled by the team.

## Team

This was a project that was known to require a lot of design work. So design was accounted for when structuring the team.
At the core of it, the following roles were filled:

* **Lead developer**: Architecture and mentoring.
* **Junior developer**: Ensure the continuity of the project, and the overall readability of the code.
* **Designer features**: Tool designer (which nodes are required, which features are needed) and demo files.
* **Designer UI**: Everything from names to shapes to colors.
* **Product manager**: Worked as Product Owner, but basically: scope, prioritization, communication, sign-off with stakeholders.
* **Scrum master**: Process (the team took that as an opportunity to try scrum as well).

In this setup, the UX was a joint effort between everyone. Some of the big picture design (e.g., Geometry Nodes are modifiers, full encapsulation, ... came from stakeholders aka the CEO).

At times there were more developers involved, but always aligned with this core structure.

### Dynamic

* **Developers**: Having more than one developer made sure the code was reviewed internally and all decisions were checked by someone else.
* **Technical artist**: Having the ultimate target audience in the team made sure all the designs were anchored on production needs and validated off the gate.
* **UI designer**: Having a centralized person to make opinionated decisions helps to give the project coherence (and personality).
* **Product manager**: In this case, the PM worked a lot as a bridge between the stakeholders and the team, avoiding surprises to both ([*](#caveat)) and building the trust to give the team more autonomy.
* **Scrum master**: Although the project moved on from scrum, it helped a lot to be forced into a framework which guaranteed a lot of shared context, syncing and overall communication and collaboration. At the core of it was a shared responsibility for all the tasks and understanding their dependencies, making sure they were clear to everyone. This framework is particularly handy for important epics which required hairy design discussions so easily postponed.

### Product ownership

Although the roles were distinct, product ownership was everyone's responsibility. In practice, it means:

* No one was too good to work on a task (doesn't matter if it is testing features or writing documentation).
* Any problem the project had was everyone's problem - no room for doing your part and being happy about it if the whole wasn't working.
* Part of this sense of ownership came from understanding that the outcome is more than the sum of the parts (outputs).

## Links

### Design presentations

* [Initial Design](https://devtalk.blender.org/t/2020-09-03-particle-workshop/15172)
* [Attributes and Fields](https://code.blender.org/2021/08/attributes-and-fields/)
* [Hair](https://code.blender.org/2022/07/the-future-of-hair-grooming/)

### Design proposals

* [Fields and Anonymous Attributes](https://devtalk.blender.org/t/fields-and-anonymous-attributes-proposal/19450) - June 2021
* [Expandable Geometry Socket](https://devtalk.blender.org/t/expandable-geometry-socket-proposal/19604) - July 2021
* [Attribute Socket Types](https://devtalk.blender.org/t/proposal-for-attribute-socket-types/19177) - June 2021
* [Enum Sockets](https://devtalk.blender.org/t/enum-sockets-proposal/21318) - November 2021
* [Anonymous Attributes, Attribute Sockets and Node Group Processors](https://devtalk.blender.org/t/anonymous-attributes-attribute-sockets-and-node-group-processors-proposal/19673) - July 2021
* [Shareable Named Attribute Node](https://devtalk.blender.org/t/shareable-named-attribute-node-proposal/20386) - September 2021
* [Explicit Instancing](https://devtalk.blender.org/t/explicit-instancing-proposal/20494) - September 2021
* [Fields and Anonymous Attributes](https://devtalk.blender.org/t/fields-and-anonymous-attributes-proposal/19450) - June 2021
* [Volumes in Geometry Nodes](https://devtalk.blender.org/t/volumes-in-geometry-nodes-proposal/31917) - November 2023

### Workshop and posts with design snippets

* [Nodes Modifier Part I: Sprites](https://code.blender.org/2020/11/nodes-modifier-part-i-sprites/)
* [Everything Nodes and the Scattered Stone](https://code.blender.org/2020/12/everything-nodes-and-the-scattered-stone/)
* [Geometry Nodes and the Melting Iceberg](https://code.blender.org/2021/02/geometry-nodes-and-the-melting-iceberg/)
* [Nodes Workshop – June 2021](https://code.blender.org/2021/07/nodes-workshop-june-2021/)
* [Geometry Nodes Icon](https://code.blender.org/2022/05/geometry-nodes-icon/)
* [Node Tools Interface and Baking](https://code.blender.org/2023/06/node-tools-interface-and-baking/)
* [Node Tools](https://code.blender.org/2023/10/node-tools/)
* [Geometry Nodes Workshop: November 2023](https://code.blender.org/2023/11/geometry-nodes-workshop-november-2023/)
