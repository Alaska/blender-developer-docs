# Blender 4.3: EEVEE & Viewport

## User Interface
- Add toggle for Fast GI Approximation, similar to Cycles. (blender/blender@000d34c398)

## Vulkan Backend (experimental)

On Windows and Linux it is possible use Vulkan to render the user interface.
This is still an experimental feature.
The goal of this release is to get feedback on compatibility and allow add-on developers
who use the `gpu` module to test their add-ons.

In the System section of the Preferences the graphics backend can be switched between
OpenGL and Vulkan.

**Hardware support**

*Windows*

- NVIDIA: GTX900 and up are supported using the latest official drivers from NVIDIA.
- AMD: RX 400 series and up are supported using the latest official drivers from AMD. 
- Intel: UHD, IRIS and Arc based GPUs should be supported using the latest drivers from Intel.
  For laptop users make sure you download the driver from the Intel website.
  Drivers provided from via laptop manufacturers might not be supported.

NOTE: At this time the Intel drivers will crash and is being investigated.

*Linux*

- NVIDIA: GTX900 and up are supported using the latest official drivers from NVIDIA.
- AMD: RX 400 series and up are supported using the latest official drivers from AMD.
  Legacy AMD GPUs are supported using mesa drivers.
- Intel: Intel UHD, IRIS and Arc based GPUs should be supported using mesa drivers.


**Limitations**

- GPU subdivision is not supported. 
- OpenXR is not supported.
- Performance can be slower compared to OpenGL. This release targets feature parity and stability.
