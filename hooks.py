import re

def on_page_markdown(markdown, page, **kwargs):
    # Avoid warnings regarding folder links on navigation.md pages.
    #
    # For actual page content we always want index.md links and show an info
    # message when not using them. This ensures links work on Gitea/GitHub too.
    #
    # But for navigation.md we have to use folder links, and this generates
    # a noisy info message. We don't actually care about generating html pages
    # from these, so just leave them blank. mkdocs-literate-nav will still
    # parse the content and generate a warning if the links is broken.
    if page.file.src_path.endswith("/navigation.md"):
        return ""

    # Replace Gitea/GitHub style commit and issue references with actual links.
    def git_hash(matchobj):
        repo = matchobj.group(1)
        sha = matchobj.group(2)
        return f"[{sha[:10]}](https://projects.blender.org/blender/{repo}/commit/{sha})"

    def issue_or_pr(matchobj):
        repo = matchobj.group(1)
        number = matchobj.group(2)
        return f"[#{number}](https://projects.blender.org/blender/{repo}/issues/{number})"

    def pr(matchobj):
        repo = matchobj.group(1)
        number = matchobj.group(2)
        return f"[PR#{number}](https://projects.blender.org/blender/{repo}/pulls/{number})"

    markdown = re.sub(r'\bblender/([a-z\-]+)@([0-9a-f]{5,40})\b', git_hash, markdown)
    markdown = re.sub(r'\bblender/([a-z\-]+)#([0-9]+)\b', issue_or_pr, markdown)
    markdown = re.sub(r'\bblender/([a-z\-]+)!([0-9]+)\b', pr, markdown)
    return markdown
