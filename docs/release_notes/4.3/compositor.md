# Blender 4.3: Compositor

## Added

- Multi-pass compositing support for *EEVEE* was added. (blender/blender@57ff2969b8)
- A *White Point* conversion mode was added to the *Color Balance* node, which works similarly to
  the *White Balance* view transform but also allows control over the target white point.
  (blender/blender@021bce8b48)
- A new global *Save As Render* option was added to the File Output node to be used if
  *Use Node Format* is enabled. (blender/blender@9c44349204)

## Removed

- The *Auto Render* option is now removed. (blender/blender@cbabe2d3ef)