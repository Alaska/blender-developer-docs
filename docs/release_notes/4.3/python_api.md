# Blender 4.3: Python API & Text Editor

### `bpy.app` Module

- Added `bpy.app.python_args` to support calling Python in an environment
  matching Blender's Python environment.
  (blender/blender@14b03fca3c3c6a24da58278373093ae9451e37c).
  _Note: this was back-ported to 4.2.1._

## Data-Blocks

- Added a new
  [`rename` function](https://docs.blender.org/api/4.3/bpy.types.ID.html#bpy.types.ID.rename)
  to the `ID` class to allow more complex ID renaming behavior. Directly assigning to the `ID.name`
  property is not affected
  (blender/blender@3e03576b09).

## User Interface

- `uiLayout.template_search()` accepts a new `text` argument to let the template add a label.
  It will respect `uiLayout.use_property_split` and `uiLayout.use_property_decorate`.

## Attribute

- Added a `domain_size` function (blender/blender@a1630792cf)
  to all the `bpy.types.AttributeGroup*`s.
  This function will return `0` if the geometry does not support the given domain.
  Example:
  ```
  >>> cube.attributes.domain_size('CORNER')
  24
  ```
- Calling `foreach_set` on attribute data will now trigger a property update
  (blender/blender@cd577f02b9).

## Curves

Added two new functions to the API (blender/blender@1672b03e84):

- `curves.remove_curves(indices=[])`:
  Removes all the curves. If a list of `indices` is provided,
  removes only the curves with these indices.
- `curves.resize_curves(sizes, indices=[])`:
  Resizes the curves to match `sizes`.
  If a list of `indices` is provided, resizes only the curves with these indices.
  In this case `sizes` and `indices` are expected to have the same length. 

## Breaking changes

### Attributes

The type `bpy.types.AttributeGroup` is replaced with one type per attribute owner:
`bpy.types.AttributeGroupMesh`, `bpy.types.AttributeGroupPointCloud`,
`bpy.types.AttributeGroupCurves`, and `bpy.types.AttributeGroupGreasePencil`.

Additionally, the properties
`active_color`, `active_color_index`, `active_color_name`, `default_color_name`, and
`render_color_index` are now only accessible on the `bpy.types.AttributeGroupMesh`.
These were already only used by meshes internally. 

### Grease Pencil

The Grease Pencil python API has been rewritten.
See the [Grease Pencil section](../4.3/grease_pencil.md#python-api-changes)
for a full list of changes and a guide on how to transition to Blender 4.3.


### Nodes

- Changing the data type of a reroute node is now done with `reroute_node.socket_idname`
  instead of modifying the socket directly.
  (blender/blender@c40dc9aa037c081bb267fc600729adf967918f7a)

## Text Editor

- Enable find "Wrap Around" by default. (blender/blender@1f95b0f43f)
- UI: Use column for find settings. (blender/blender@d254a12be9)
