# Blender 4.3: Pipeline, Assets & I/O

## USD
- Point cloud export is now supported
  (blender/blender@460aa3c231321ebfc8208ab65f18826066f72c60)
- Improved handling of `@asset@` paths inside USD files
  (blender/blender@8a97f31e7694908fadee3db2305488250bbd63cf)
- More efficient export of animated attribute data when unchanged from prior frames
  (blender/blender@3c394d39f2ced658d990f437981f1d855917b1cb)
