# Google Summer of Code - 2019 Projects

**Progress updates for 2019**

## Current State

Results for the projects have been announced
[here](https://code.blender.org/2019/09/google-summer-of-code-2019/).

**1<sup>st</sup> evaluation -** 24 - 28 June
**2<sup>nd</sup> evaluation -** 22 - 26 July
**Final submission -** 19 - 26 August

## Communication

### Development Forum

All Blender and code related topics will go to the devtalk forum. Here
students will also post their weekly reports, and can ask for feedback
on topics or share intermediate results with everyone.

<https://devtalk.blender.org/c/blender/summer-of-code>

### chat.blender.org

For real-time discussions between students and mentors. Weekly meetings
will be scheduled here as well.

<https://chat.blender.org>

## Projects

Google has granted 8 projects for the Blender Foundation.

------------------------------------------------------------------------

### Continued development on LANPR engine for Blender

by **Yiming Wu**
*Mentors:* Sebastian Parborg, Clement Foucault

- [Proposal](https://archive.blender.org/wiki/2024/wiki/User:Yiming/GSoC2019/Proposal.html)
- [Updates](https://archive.blender.org/wiki/2024/wiki/User:Yiming/GSoC2019/Updates.html)
- [Summary](https://archive.blender.org/wiki/2024/wiki/User:Yiming/GSoC2019/Summary.html)

------------------------------------------------------------------------

### Intel Embree BVH for GPU

by **Quentin MATILLAT**
*Mentors:* Sergey Sharybin, Stefan Werner

------------------------------------------------------------------------

### Improve Cycles/EEVEE For Procedural Content Creation

by **Omar Ahmad**
*Mentors:* Jacques Lucke, Brecht Van Lommel

- [Proposal](https://archive.blender.org/wiki/2024/wiki/User:OmarSquircleArt/GSoC2019/Proposal.html)

------------------------------------------------------------------------

### Outliner Improvements

by **Nathan Craddock**
*Mentors:* Campbell Barton, Brecht van Lommel

- [Proposal](https://archive.blender.org/wiki/2024/wiki/User:Zachman/GSoC2019/Proposal.html)
- [Plan and Schedule](https://archive.blender.org/wiki/2024/wiki/User:Zachman/GSoC2019/Plan.html)
- [Final Report](https://archive.blender.org/wiki/2024/wiki/User:Zachman/GSoC2019/Report.html)

------------------------------------------------------------------------

### Core Support of Virtual Reality Headsets through OpenXR

by **Julian Eisel**
*Mentors:* Dalai Felinto, Sebastian Koenig

- [Proposal](https://archive.blender.org/wiki/2024/wiki/User:Severin/GSoC-2019/Proposal.html) (Updated)
- [How to Test](https://archive.blender.org/wiki/2024/wiki/User:Severin/GSoC-2019/How_to_Test.html)
- [Final Report](https://archive.blender.org/wiki/2024/wiki/User:Severin/GSoC-2019/Final_Report.html)

------------------------------------------------------------------------

### Cloth Simulator Improvement

by **Ish Hitesh Bosamiya**
*Mentors:* Brecht Van Lommel, Jeroen Bakker

- [Proposal](https://archive.blender.org/wiki/2024/wiki/User:Ishbosamiya/GSoC2019/Proposal.html)
- [Weekly
  Reports](https://devtalk.blender.org/t/gsoc-2019-cloth-simulator-improvement-weekly-reports/7726?u=ish_bosamiya)
- [Final Report](https://archive.blender.org/wiki/2024/wiki/User:Ishbosamiya/GSoC2019/FinalReport.html)

------------------------------------------------------------------------

### Fast Import / Export for OBJ, STL and PLY formats

by **Hugo Sales**
*Mentors:* Jacques Lucke, Sybren Stüvel

- [Proposal](https://archive.blender.org/wiki/2024/wiki/User:Someonewithpc/GSoC2019/Proposal.html)

------------------------------------------------------------------------

### Bevel Custom Profiles

by **Hans Goudey**
*Mentors:* Howard Trickey, Rohan Rathi

- [Proposal](https://archive.blender.org/wiki/2024/wiki/User:HooglyBoogly/GSoC2019/Proposal.html)
- [Weekly
  Reports](https://devtalk.blender.org/t/gsoc-2019-bevel-profiles-weekly-reports/7651/21)
- [Log](https://archive.blender.org/wiki/2024/wiki/User:HooglyBoogly/GSoC2019/Log.html)
- [Notes](https://archive.blender.org/wiki/2024/wiki/User:HooglyBoogly/GSoC2019/Notes.html)
- [Final Report](https://archive.blender.org/wiki/2024/wiki/User:HooglyBoogly/GSoC2019/Final_Report.html)
