# Blender 4.2 LTS: User Interface

## General

- Support for horizontal rules in vertical layouts, vertical rules in horizontal.
  (blender/blender@4c50e6992f)
- Improved user interface scaling.
  (blender/blender@85762d99ef, blender/blender@f85989f5db, blender/blender@196cd48aad,
  blender/blender@23aaf88233, blender/blender@1f1fbda3ee)
- Icon changes. (blender/blender@42e7a720c9, blender/blender@4e303751ff, blender/blender@7b601464c3)
- Optimizations to the font shader. (blender/blender@a05adbef28)
- Interface font updated to Inter 4.0. (blender/blender@f235990009)

![Inter font](images/Inter.png){style="width:400px;"}

- Corrected coloring when hovering inactive UI items. (blender/blender@5bed08fc56)
- Tooltip changes. (blender/blender@3d85765e14, blender/blender@08cd80a284)
- Improved square color picker when shown very wide. (blender/blender@857341a7a7)
- Tooltips for colors featuring large sample and values. (blender/blender@41bbbd2359)

![Color tooltips](images/ColorTooltip.png){style="width:400px;"}

- Asset Browser now supports Maximize Area. (blender/blender@2d5a83f294)
- Wavelength node now uses nanometer-scale inputs. (blender/blender@cc541f2c07)

## Status Bar

- Improvements to Status Bar keymap display. (blender/blender@c4e5a70e07, blender/blender@44606c5e4d, blender/blender@42a8947eb1, blender/blender@dfe060ea6f, blender/blender@03cd8b359b, blender/blender@3fbc603137, blender/blender@1391b7de41, blender/blender@02798f8438)

![Status bar keymap](images/StatusBarKeymap.png){style="width:650px;"}

- Status Bar now collapses multiple X/Y/Z operations to a more concise format. (blender/blender@49bd285529)

![Status bar keymap](images/XYZCollapse.png){style="width:400px;"}

- Expanded Status Bar icons for keymap events. (blender/blender@d1b6621903)

![Keymap icons](images/KeymapIcons.png){style="width:500px;"}

- Tighter Status Bar keymap spacing. (blender/blender@65bfae2258)
- Status Bar mouse event icons are better aligned. (blender/blender@9cb298f706)

## Dialogs

- Improved large confirmation dialogs.
  (blender/blender@00b4fdce2c, blender/blender@6618755912, blender/blender@3bacd93ba6,
  blender/blender@2c8a3243ee, blender/blender@5eda7c9fd3, blender/blender@311e0270dd,
  blender/blender@d3798970199a6b58efc3701785829d17561ea352)

![Large confirmation](images/LargeConfirmation.png){style="width:500px;"}

- Improved small confirmation dialogs. (blender/blender@443ea628c5, blender/blender@529b8786f8)

![Small confirmation](images/SmallConfirmation.png){style="width:400px;"}

- Improved property dialogs.
  (blender/blender@8fee7c5fc7, blender/blender@da378e4d30, blender/blender@615edb3f3c)
- Panels now supported in dialog popups. (blender/blender@aa03646a74)
- Some operator confirmations removed. (blender/blender@6dd0f6627e, blender/blender@7226b52728)
- Improved confirmation when reverting the current file. (blender/blender@75a9cbed24, blender/blender@8735a8a6be)

## Menus
- Recent Items context menu now supports opening file location. (blender/blender@6789a88107)
- "Select" menus reorganized to be more consistent. (blender/blender@ede675705e, blender/blender@4dbca26b5c)
- Menus in mesh sculpt mode were reorganized after gaining new items (blender/blender@1fe8fbd93a)
- Changes to menu separator padding. (blender/blender@48dee674f3)
- All Zoom menus now use a new consistent format. (blender/blender@7e2075b809)

![Zoom menus](images/ZoomMenu.png)

## 3D Viewport

- Ability to toggle Camera Guides in Viewport Overlays. (blender/blender@392ac52ebb)
- Ability to toggle Camera Passepartout in Viewport Overlays. (blender/blender@2a287dc23c)
- Corrected initial viewport tilt. (blender/blender@913acaf395)
- More improvements to overlay text contrast. (blender/blender@3582e52f9c, blender/blender@3ee8a75e8e)
- Reduced dead zone for 3Dconnexion SpaceMouse. (blender/blender@d739d27a2d)
- Object menu gains a Modifiers submenu. (blender/blender@e63c8bb3c2)
- Sidebar made a bit wider. (blender/blender@068e624ebc)

## Properties Editor

- Add, remove, apply, and reorder modifiers of all selected objects by holding alt. (blender/blender@9a7f4a3b58d9)
- Changes to Modifer pinning. (blender/blender@7ee189416c)
- Add eyedropper button to camera focus distance. (blender/blender@7f8d4df410)

![Focus distance eyedropper](images/FocalDistancePicker.png){style="width:400px;"}

## Text Editor

- GLSL syntax highlighting. (blender/blender@462c144f414343ffbbac3546f1fae2bbf0bd52db)
- Drop strings & URLs into the text editor and Python console
  (blender/blender@64843cb12d543f22be636fd69e9512f995c93510)
![Drag URLs and texts to text and console editors](images/DragToTextAndConsole.png){style="width:500px;"}


## Node Editor
* Input sockets that don't allow editing the value don't show up in the sidebar anymore.
  (blender/blender@d27a1c47fafb84ebf0649423102a9e79b9a201b7,
  blender/blender@d9d9ff1dcd2036997064f982bbb94cf7d52e4828).
* Slightly improved sorting in search when adding nodes (blender/blender@d1634b2a4aae,
  blender/blender@22c6831f63ad).
* Show tooltip with node description when over node title (blender/blender@749433f20bc5bd893fee0c9fcb2d20578a67d224).
* Improved tooltips for dangling reroute nodes (blender/blender@fa66b52d0ae6).
* Improved tooltips for multi-input sockets (blender/blender@ed9921185abb).
* Display node label for tooltips on reroute nodes (blender/blender@0bd627950436).
* Multiple images can be added at the same time using drag and drop (blender/blender@615100acdaba).
* Node groups can have descriptions now (blender/blender@6176e66636342dabbd78594ecfb188c8a4bc996c).
* Node group properties have been reorganized (blender/blender@125afb20d330a27a51b73cd4f55cd4136ea71fbb).
* Node groups now have a color tag that affects the header color of group nodes (blender/blender@f4b9ca758a10dbc295f608f1a56c97397c8dd884).
* When adding a frame around the selected nodes (ctrl+J), the new frame will now be added to the common root frame of these nodes (blender/blender@0f32291d15ef2e076adcb63eec5aa32681b548e7).
* Invalid (red) links now have some additional information for why they are invalid (blender/blender@399f6a1c6056bee6fd4f311baee66e69ba2b2a88).
* Dragging a node onto a link does not remove the link anymore if it is incompatible. It's dimmed instead (blender/blender@ebb61ef30f716918592c310568b2d154dfde72a4).
* Resetting socket values now actually works, instead of always setting values to zero (blender/blender@6b6657405f69c86e280d3d0bf5547b9834d39f6e).
* Using the shortcut (ctrl+shift+click) to connect to a viewer now also moves the viewer closer to the node (blender/blender@7be4d4f443a82a589ec47238765fc16aa9934246).
* Some sockets can now be renamed directly in the node by Ctrl+Clicking on it (blender/blender@cd5eb4aa03ee93279d16d4f36ca09a2fc7717dff).
* New overlay to reroute labels that are derived from upstream reroute nodes (blender/blender@db5d41016446467be11174a7e74a9f836fea4826).


## Outliner

- Ctrl-F can initiate Outliner filtering. (blender/blender@f5fdda776c)
- Orphan Data mode renamed to Unused Data. (blender/blender@72be662af4)
- Blender File mode now supports managing user counts. (blender/blender@d8f6ae7919)

![Blender File mode](images/OutlinerFileMode.png){style="width:500px;"}

## Unused Datablocks

The Purge operator now pops up a dialog where user can choose which options to apply,
and get instant feedback on the amounts of unused data-blocks will be deleted.
(blender/blender!117304) (blender/blender@0fd8f29e88)

![New purge popup](images/ui_new_purge_popup.png){style="width:500px;"}

The File > Cleanup menu has a new Manage Unused Data operation to open the outliner
in Unused Data mode. (blender/blender@f04bd961fd)

## Preferences

- New button to save custom themes. (blender/blender@ee38a7b723)

![Save theme](images/SaveTheme.png){style="width:500px;"}

- Improved operators for adding/deleting custom themes. (blender/blender@a0a2e7e0dd)
- Improved operators for adding/deleting custom key configurations. (blender/blender@57729aa8bb)
- Preferences "Translation" section changed to "Language". (blender/blender@73d76d4360)

## Splash

- Changes to Splash Screen for new versions. (blender/blender@d2d810f11a)

![Splash screen changes](images/SplashChanges.png){style="width:500px;"}

## Platforms

- Linux: Registering & unregistering file-type associations is now supported on Linux.
  This can be accessed via the system preferences "Operating System Settings" or via
  command line arguments (`--register`, `--register-allusers`), see `--help` for details.
  (blender/blender@9cb3a17352bd7ed57df82f35a56dcf0af85fbf03).
- Linux: Image copy and paste now works in Wayland. (blender/blender@692de6d380)
- Windows: Fixed pixel shift in clipboard image paste. (blender/blender@a677518cb5)

## Translations

- The ability to edit PO files directly from the user interface has been removed. ([details](https://devtalk.blender.org/t/ui-translation-tools-remove-edit-translation-feature-from-blender-ui-in-4-2lts-release/34947), blender/blender@b143cc1885)

