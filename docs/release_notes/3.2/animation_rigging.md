# Animation & Rigging Module

## General

- Setting key shortcuts from the "Insert Keyframe" menu is now supported
  (blender/blender@cda33345865a2705e0cbe24448865b2bdfc7ead6).
- F2 now also works for renaming markers
  (blender/blender@f1ae6952a86b).
  The old Ctrl+M key has been changed to F2, to match renaming of
  objects and NLA strips (see below).
- Markers are easier to select
  (blender/blender@f1ae6952a86b),
  with new operators to select all of them, or left/right of the current
  frame (including the current frame, if it's on a marker).
- Discontinuity (Euler) Filter is now available in the Dope Sheet as
  well
  (blender/blender@7fd11d9c7f5b).
  As in the graph editor, it still requires the animation channels (the
  FCurves) themselves to be selected.
- Equalize Handles now can change the handle types, so that it always
  performs its job
  (blender/blender@eed8c2e65554).
- Inverse Kinematics (IK) chain length is now no longer animatable
  (blender/blender@7b62203fc794).

## Proxy Removal

The proxy system has been fully removed, see [the Core release
notes](core.md) for details.

## NLA Editor

- F2 now works for renaming NLA strips
  (blender/blender@150f42e6d3a1).
- Strip extrapolation modes "Hold" and "Hold Forward" will remain as set
  (blender/blender@63d2980efa2).
  Blender used to change these settings, only allowing "Hold" for the
  very first NLA strip, changing it without notice to "Hold Forward"
  whenever an animator would change the strip order and put another one
  in front. This behaviour has been removed, and the settings will stay
  at the values set by the animator.
- Bake Action is available from the Edit menu
  (blender/blender@c63d64a2ce3e).
- Action influence is now copied to the Strip influence when pushing
  down to the NLA
  (blender/blender@2f49908a5e57).
- An alternative Tweak mode is now available, `Start Tweaking Strip Actions (Full Stack)`
  (blender/blender@3acbe2d1e933).
  The old behavior can be found using `Start Tweaking Strip Actions (Lower Stack)`
  and remains the default for the TAB hotkey.
  This new mode allows animators to enter tweak mode without disabling all the
  tracks above the tweaked strip, effectively animators will see the "Full Stack".
  Keyframing will work as expected, preserving the pose that you visually keyed.
  *There are some limitations, so make sure to read the commit message.*

## Motion Paths

When adding Motion Paths you can now select more sensible frame ranges
(blender/blender@4e57b6ce77b1),
rather than simply defaulting 1-250:

- Scene Frame Range
- Selected Keyframes
- All Keyframes

The scene range operator was removed, because the operators now
automatically find the range when baking the motion paths.

## Deprecation of Legacy Pose Library

The legacy Pose Library (Blender 2.93 and older) has been marked as
deprecated
(blender/blender@a824c076b7b2
and
blender/blender@81b55d7988c1).
The panel and its operators will be removed from Blender 3.3, along with
the internal code supporting the functionality. The panel gained some
explanation about this, a button that opens the manual at the relevant
chapter, and a button to convert legacy pose libraries into modern pose
assets.

![Pose Library panel with deprecation notices](../../images/blender-3.2-poselib-deprecation.png){style="width:350px;"}

![Animation editor with conversion button to modern pose assets](../../images/blender-3.2-convert-old-poselibs.png){style="width:800px;"}
