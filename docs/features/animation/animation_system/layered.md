# Layered Animation

!!! WARNING "Work In Progres"
    The Layered animation system is currently work in progress. The goal for Blender 4.4 is to have
    "Slotted Actions" (see below) but not yet a fully layered animation system.

Relevant links:

- [2024: Slotted Actions planning][phase1] (projects.blender.org)
- [2024: Animation 2025: Progress & Planning][blogpost-baklava] (code.blender.org)
- [2023: Layered Animation workshop][workshop-2023] (code.blender.org)
- [2022: Character Animation workshop][workshop-2022] (code.blender.org)
- [Layered Animation: Open Questions](https://hackmd.io/@anim-rigging/SkNZ9pLKp) (on HackMD)

[phase1]: https://projects.blender.org/blender/blender/issues/120406
[blogpost-baklava]: https://code.blender.org/2024/02/animation-2025-progress-planning/
[workshop-2023]: https://code.blender.org/2023/07/animation-workshop-june-2023/
[workshop-2022]: https://code.blender.org/2022/11/the-future-of-character-animation-rigging/

## Main Goals

In short, the work on "layered animation" brings two new features to the `Action` data-block:

- **Slots:** Multiple data-blocks can be animated by a single `Action` (and still have different animation).
- **Layers:** Animation can be stored in layers on an `Action` (rather than having separate NLA data-structures per animated data-block).

## Design

For the broad design, see the [2023 workshop][workshop-2023] blog post. Since
then some things were changed / detailed, which are described here.

### Phase 1: Multi-data-block Animation

The first phase of the work will focus on multi-data-block animation. This makes it possible to have
related data-blocks share an Action (so a single box to put all the animation in). Examples are:

- Mesh transform (object) and shape key values (belongs to the object data).
- Camera transform (object) and lens parameters (object data).
- Metaball transforms, where all objects in a group of metaballs can share the same Action.

The initial version of the "slotted Actions" system will *not* have layers or strips, at least not
from the user/UI perspective. Under the hood animation data is still stored on a layer with a single
strip. These are all implicitly created, though. This makes the whole UI/UX design a lot simpler,
and allows us to focus on the multi-data-block animation part first.

For an up to date plan, see [Slotted Actions Planning][phase1].

#### Dividing Actions with Slots

An Action is a container that holds the animation data for one or more data-blocks. Data-blocks in
Blender represent various types of data such as objects, armatures, or materials, which can all be
animated.

To differentiate the animations for each data-block within an Action, the concept of *Slots* is used.
Each Slot within an Action corresponds to a specific data-block, allowing for organized and precise
animation control. This division ensures that animations for different data-blocks do not interfere
with each other and can be managed separately.

#### Linking Data-blocks to Actions and Slots

Data-blocks are linked to Actions to define their animations. Each data-block points to the Action
that contains its animation data and specifies which Slot within that Action it uses. This setup
allows for a flexible animation system where multiple data-blocks can share the same Action but
still have distinct animations through their respective Slots.

#### Action Independence

It is important to note that an Action itself does not decide what it will animate. Instead, it is
the data-blocks that reference an Action and choose the appropriate Slot to define their animation.

#### Sharing Slots Across Data-blocks

In some cases, multiple data-blocks can share the same Slot. When this occurs, the data-blocks will
be animated identically, as they are referencing the same set of animation data within the Action.
However, sharing the same Slot is considered a corner case and is not expected to be widely used. It
can be useful in specific scenarios where identical animation across multiple data-blocks is
desired.


### Phase 2: Layered Animation

Once Slotted Actions have landed in Blender, and animators have had time to get used to it, the next
phase of the work will bring layered animation to the `Action`. There will likely be other work done
between the two phases, in order for Phase 1 to receive the necessary feedback and subsequent
adjustments.

### Channel Groups & F-Curve Ordering

Legacy `Action`s are separated into 'action groups'. These are arbitrarily named
groupings of F-Curves. This has certain downsides:

- **Grouping:** Some tooling assumes that these are *not* arbitrarily named, and
  makes the assumption that if they match a bone name, all the animation for
  that bone is inside that group. These assumptions can fail, as it's possible
  to arbitrarily rearrange F-Curves and even pop them out of a group, or group
  them manually.
- **F-Curve Order:** F-Curves can be manually reordered within the Action. This
  makes it rather complex for the Blender code to efficiently, say, get all
  rotation components, as this requires a full scan of all the F-Curves.

Proposed design for F-Curves in an `Animation` data-block:

**Grouping** would (for now) be hard-coded and automatically managed by Blender.
These groups would have to exist in DNA in order to store things like
expanded/collapsed state, selection state, and color (for example for bones). It
shall *not* be possible to manually add groups or to move F-Curves between
groups.

Blender would create the following groupings:
  - Property groups: "Transforms", "Custom Properties", maybe some other
    classification of properties.
  - Sub-data groups: Bone name, maybe other sub-data as well.

**F-Curve Order:** F-Curves should be ordered by group, and within that group by
RNA path and array index. This makes it predictable (for both animators and
Blender itself) where they are and how they're ordered. For example, the current
Euler Filter operator assumes that F-Curves for rotation components are
sequentially stored. In practice this is true most of the time, but not always.
Enforcing a fixed ordering will make these operations faster and more
predictable. It is also unclear how much advantage manually reorderable F-Curves
bring to animators.

For groupings of sub-data, the group order should follow the sub-data order. For
bones this would simply be alphabetical, but for shapekeys it should follow the
shapekey order. It is unclear whether this reordering would always happen when
reordering the sub-data, or whether it would be some button to press to sync up
the ordering. Probably the former is more desirable.

**Ordering of Groups** manually should likely still be possible. That way an
animator can, for example, order bones in a sensible-for-the-rig way. A common
example is to order the fingers in actual finger order, instead of
alphabetically.
