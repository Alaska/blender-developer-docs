# Add-ons

Blender builds and releases include python script add-ons that extend
its functionality. This page has information for developers who want to
have their add-on bundled with Blender.

**Mandatory prerequisites to get an add-on accepted**

You must be aware that your add-on will be integrated into the official
Blender distribution. Therefore you must ensure there is continuous
support for keeping your add-on working and fixing bugs as they appear.
So you must commit yourself (or find a maintainer who does it for you)
to do the maintenance (see *General Guidelines for Add-on Developers*
below)

- [Add-ons
  Repository](https://projects.blender.org/blender/blender-addons) on
  projects.blender.org
- Submit an add-on by making a [pull
  request](../contributing/pull_requests.md) for this repository

## Key requirements for Add-ons

We want to implement fair and uniform rules for all add-ons included in
Blender releases.

- Base line is that each add-on that gets bundled, is meant to function
  at the same quality level as other Blender features. What we require
  from C/C++ we also require from Python scripts, not only technically,
  but also following Free Software principles.

<!-- -->

- Add-ons in a release are fully functional and useful additions to
  Blender, not requiring or advertising extensions or options available
  outside of blender.org.

<!-- -->

- While there is a lot of room to extend Blender in different ways,
  please note that functionality or tweaks specific for your own
  workflow, or only minor changes to existing tools may be rejected for
  inclusion in Blender. We decide on a case by case basis if an add-on
  gets included or not. Getting your add-on tested by users helps here.

This implies that:

- Blender does not include add-ons in a release that require internet to
  work, unless it connects to blender.org based on explicit user
  permission. For such (new) add-ons we will start a new module on
  blender.org.

<!-- -->

- Add-ons in Blender releases offer the full documentation on
  blender.org.

<!-- -->

- Blender does not include add-ons in a release that require installing
  additional modules.

<!-- -->

- Blender does not include add-ons in a release that create a bridge to
  non-free (non GPL compliant) software.

## Contact

- [chat.blender.org #python room](https://chat.blender.org/#/room/#python:blender.org).

## Repositories

Add-ons are hosted in two Git repositories, that are automatically
checked out with the main Blender repository when running make update.

- [Blender
  Add-ons](https://projects.blender.org/blender/blender-addons):
  included in official releases on blender.org, strictly reviewed
- [Blender Add-ons
  Contrib](https://projects.blender.org/blender/blender-addons-contrib):
  included in test builds in the Testing tab, for more adventurous users

They can be found in these two directories of the main Blender
repository:

`scripts/addons`
`scripts/addons_contrib`

## Submitting an Add-on

- Follow the [Guidelines for
  add-ons](guidelines/index.md) when implementing it.
- Ensure your addon is publicly available and well documented.
- Submit the add-on as a pull request with the relevant links to your
  addon and its full documentation.
- Try to get your add-ons tested by users, gather feedback and improve
  as needed.

The reviewer will decide if the add-on can be included in official
release, included in the Contrib repository or can't be included. If the
add-on is in the Contrib repository and matures, you can later message
in the \#python channel on chat.blender.org and ask for the add-on to be
moved to the repository for official releases.

If the add-on is on track to be included in official releases, you may
be asked to submit it to our [Code Review](../contributing/pull_requests.md)
tool for a closer review.

## General Guidelines for Add-on Developers

If you attempt to submit your add-on either to the contrib section or to
the release section of the add-on Repository, then your add-on will
undergo a review process. Within this process changes may be requested
to keep inline with good coding practices. Those changes may be provided
by you or by the reviewer. This will be clarified with the reviewer on a
case by case basis.

Once your add-on is accepted and hosted in the Blender release section
or the contrib section, you become responsible for maintaining your
add-on and you get commit permissions to the add-on repository...

## Duties of an Add-on maintainer

As soon as you got the commit permissions you...

#### Must Do

- Maintain and further improve your add-on by yourself.
- Ensure your add-on keeps working with new Blender Releases and API
  changes. Therefor check regularly & keep up to date in between Blender
  Official release to ensure your add-on is working in the next release
  of Blender.
- Handle bug reports from users.
- When you add new features then you also should add brief documentation
  and explanations to the Blender release notes.
- Check regularly for patches to your add-on from other users and
  eventually commit updates they make using the current git version of
  the add-on as the base.

**Avoid unmaintained Add-ons**

Add-ons that are not supported (or no longer supported) by their authors
may be removed from Blender if they are broken & we can not find a
maintainer.

#### Should Do

- Write good commit messages that explain which add-on and what exactly
  you change ([good
  example](http://markmail.org/message/flgr4pums6laqklv)).
- It's acceptable to test other developers’ add-ons, and commit small
  fixes to their code (like a typo or obvious bug fixes).
- Optional: make your script [pep8
  compliant](http://www.python.org/dev/peps/pep-0008/) and follow [best
  practice](http://www.blender.org/documentation/blender_python_api_2_68a_release/info_best_practice.html).

#### Never Do

- Commit code that is not GPL2+ or compatible with it.
- Commit code you are not the author of (unless you have the author’s
  authorization).
- Commit big changes to other add-ons (unless you get the author’s
  permission).
- Commit binary files like .exe, .pyc, .pyd, .dll, .whl, etc.
- Add commit logs in your code (Git logs should be enough)

## Working with Git

The same advice from our [Git Usage](../contributing/using_git.md) page applies here.

If you have a full Blender checkout, you can go into the
`release/scripts/addons` or `release/scripts/addons_contrib` directory,
and it's as if you are working in the Blender Add-ons or Blender Add-ons
Contrib repositories directly. You can do all the typical git commands
there for adding files, committing changes, and fetching and rebasing
updates.

Optionally you can choose to clone the individual repositories and work
there:

``` bash
git clone https://projects.blender.org/blender/blender-addons.git
git clone https://projects.blender.org/blender/blender-addons-contrib.git
```

## Bug Reports

Bugs for all add-ons are reported to the add-ons repository on
projects.blender.org. If a bug is filed for your add-on, the bug will be
assigned to you as the add-on maintainer.

You can also help out with bugs that are marked as Needs Triage in the
tracker, to assign them to the right maintainer, confirm the problem or
even fix the bug yourself.
