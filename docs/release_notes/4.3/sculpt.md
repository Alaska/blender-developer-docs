# Blender 4.3: Sculpt, Paint, Texture

## Brushes

Brushes are now stored in asset libraries shared across projects,
instead of having copies in each blend file.

The built-in brushes are in the *Essentials* library shipped with Blender.
Brushes can be created by duplicating an existing built-in brush.
Each brush is saved in its own blend file in the user asset library,
with textures and images packed.

The asset shelf is the new place to select brushes, instead of the toolbar.
Brushes are organized into asset catalogs,
instead of being grouped by tool.

The asset shelf appears horizontally across the bottom of the 3D Viewport and
Image Editor in sculpt and paint modes.
It can be configured to show a subset of catalogs, depending on the needs of the project.
Alternatively, brushes can be selected in a popup from the tool settings.

![Brush Asset Shelf](images/sculpt-paint_brush_introduction_brush-asset-shelf.png)

Refer to the [user manual](https://docs.blender.org/manual/en/4.3/sculpt_paint/brush/introduction.html)
to learn about the new asset based workflow.

### Converting Brushes

To make existing brushes available, save them in a blend file in the user asset library folder.
By default this folder is `Documents/Blender/Assets`.
Then in the outliner with Blender File view, right click each brush and Mark as Asset.

## Performance

Brush evaluation, viewport drawing, and many other parts of sculpt mode were almost completely
rewritten to solve problems, improve performance, and ease future development.
A full list of the work is available the
[development task](https://projects.blender.org/blender/blender/issues/118145).

- Entering sculpt mode is about 5 times faster
  (a change from 11 to 1.9 seconds with a 16 million face mesh with a Ryzen 7950x).
- Brush evaluation is about 8 times faster for mesh sculpting
  (a benchmark change from 2.9 to 0.36 seconds sculpting most of a 6 million face mesh).

## General
- The Voxel Size operator now works in relative mode by default.
  (blender/blender@4221f827cf1b7c25c0cb2b47cca754ec11540fa9)
- The lasso tools now have optional stroke stabilization
  ([manual](https://docs.blender.org/manual/en/4.3/sculpt_paint/sculpting/introduction/gesture_tools.html#tool-settings))
  (blender/blender@9d4d1aea98e1b30278a337f440f9962afe35bf2a).
- A polyline gesture can now be finished with double-click
  (blender/blender@64211aa8c61da7473c5ff78f405df5aaafb2c195).
- Sculpt mask is now accessible in node tools (blender/blender@98f0926825d7).
  
## New Tools
- *Mask from Boundary* operator added to modify mask values based on mesh or face set islands
  (blender/blender@591f4c0e59dd132f3ee3a74cb1c3908bdd58016c).
