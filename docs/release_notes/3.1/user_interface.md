# Blender 3.1: User Interface

## General

- Preferences / File Paths / Fonts now starts with an initial default
  path for Mac & Linux, as already done for Windows.
  (blender/blender@02a9377da0da).
- Open File Browser in thumbnail view when browsing for fonts.
  (blender/blender@9cfffe84681b).
- View pie menu for Node Editor, Video Sequencer, Dopesheet, Graph, NLA,
  Image, Clip and Outliner.
  (blender/blender@e1bd4bbb6668).
- Font point sizes can now be set using floating-point values.
  (blender/blender@73047c69ea80).
- The curve mapping widget layout has been slightly tweaked for
  consistency
  (blender/blender@8c55481e337c9c).
- Improved feedback while doing interactive (live) area splitting
  (blender/blender@fa8c2c788579).
