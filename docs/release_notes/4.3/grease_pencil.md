# Blender 4.3: Grease Pencil

Grease Pencil was rewritten to remove deeper limitations and improve overall performance.

## New Features
While the focus of the rewrite was compatibility and feature parity with Blender 4.2,
there are some new features that come with this release.

### Layer Groups
*TODO*

### Draw Tool Changes
The behavior of the draw tool has been updated.

#### Radius Unit
*TODO*

#### Active Smoothing
*TODO*

#### Screen Space Simplify
*TODO*

### Eraser Tool Changes
*TODO*

### Geometry Nodes
*TODO*

### Fill Gradient tool

A new tool to edit the fill gradient was added. It can be found in Edit Mode in the tool bar below
the Interpolation tool.
Users can change the fill gradient (defined in the material settings) by selecting the fill strokes
and then click-dragging with the Gradient tool active.
(blender/blender@1b6220a4baf551dabc383df45f564da3208093a8)

## Performance Improvements
*TODO*

## Miscellaneous Changes
*TODO*

## Conversion & Compatibility
From 4.3 and onward Grease Pencil objects are automatically converted to the new architecture on
file load.
There is no backwards compatibility, e.g. Grease Pencil files created or saved in 4.3 or higher
will not load correctly in 4.2 or lower.

## Deprecated Features
Some features present in 4.2 have been removed.
This is mostly due to the changes in the architecture and the difficulty in maintainability. 

### Screen Space Stroke Thickness
Strokes are now always in "World Space" thickness mode.

Note that it is possible to recreate the "Screen Space" thickness effect using a geometry nodes
modifier.

### Interpolation Tool Selection Order
The selection order (in edit mode), that was used to specify pairs of strokes to interpolate, has
been removed.
Porting this feature was not trivial, so it was decided to replace it with a better version in the
future.

### Drawing Guides
The guides in Draw Mode have not been ported to 4.3. There were multiple standing issues that made
it difficult to convert the code without introducing more bugs. It was decided that this will be
replaced with a more capable system in a future release.

## Python API Changes

The Python API for Grease Pencil was rewritten to accommodate for the new data structure.
For add-on developers wanting to migrate their add-ons to 4.3,
See **[python migration process](grease_pencil_migration.md/#python-api)** for more details.
