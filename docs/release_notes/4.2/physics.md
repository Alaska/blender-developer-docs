# Blender 4.2 LTS: Physics

## Physics

* Fluid domains now have liquid meshing enabled by default, so that they are visible in renders. For visualizing the particles better, this option can be disabled. (blender/blender@a36d7774cc2b60f4b6df5702f74fad334055b1ea)
