# User Interface Module

See the [**Module Landing
Page**](https://projects.blender.org/blender/blender/wiki/Module:%20User%20Interface)
for general information.

- [Getting Involved as a Designer](get_involved_as_designer.md)

## Release Notes

- [Blender 2.93 LTS - May 26, 2021](../../../release_notes/2.93/user_interface.md)
- [Blender 2.92 - February 24, 2021](../../../release_notes/2.92/user_interface.md)
- [Blender 2.91 - November 25, 2020](../../../release_notes/2.91/user_interface.md)
- [Blender 2.90 - August 31, 2020](../../../release_notes/2.90/user_interface.md)
- [Blender 2.83 LTS - June 3, 2020](../../../release_notes/2.83/user_interface.md)
- [Blender 2.82 - February 14, 2020](../../../release_notes/2.82/ui.md)
- [Blender 2.81a - November 21, 2019](../../../release_notes/2.81/ui.md)
- [Blender 2.80 - July 30, 2019](../../../release_notes/2.80/ui.md)
- [Blender 2.79b - September 11, 2017](../../../release_notes/2.79/ui.md)
