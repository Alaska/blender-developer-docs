# Blender 4.1: EEVEE

## Light Probes

Probe types have been renamed:

* Reflection Cubemap to Sphere
* Reflection Plane to Plane
* Irradiance Grid to Volume
